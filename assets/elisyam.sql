-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 12, 2018 at 08:28 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `elisyam`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `updatedate` datetime NOT NULL,
  `createdate` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='ตารางจัดเก็บข้อมูลกิจกรรมล่าทุน The Hunter Game';

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `tel`, `email`, `password`, `status`, `level`, `active`, `updatedate`, `createdate`, `deleted`) VALUES
(1, 'test', 'test', 'test', 'test@mail.com', '', 0, 0, 0, '2018-10-04 07:42:29', '2018-10-04 07:42:29', 0),
(2, 'test', 'test', '025555555', 'test@mail.com', '', 0, 0, 0, '2018-10-04 09:33:48', '2018-10-04 09:33:48', 0),
(3, 'Test', 'test', 'test', 'test@mail.com', '', 0, 0, 0, '2018-10-04 10:03:31', '2018-10-04 10:03:31', 0),
(4, 'T', 'S', '0894444444', 'thajchalai.s@bu.ac.th', '$2y$12$qn7CEMtUeRe3mYO8SHuI5O73ZJGtl5r9HSYpkjth419vV8aXeYK9K', 0, 0, 0, '2018-11-12 14:21:05', '2018-10-04 15:08:33', 0),
(5, 'test', 'test', 'test', 'test@mail.com', '', 0, 0, 0, '2018-10-04 15:26:32', '2018-10-04 15:26:32', 1),
(6, 'test', 'test', 'test', 'test@mail.com', '', 0, 0, 0, '2018-10-04 15:27:57', '2018-10-04 15:27:57', 1),
(7, 'Test', 'Test', '0894423614', 'kiksee@hotmail.com', '', 0, 0, 0, '2018-10-04 15:50:56', '2018-10-04 15:50:56', 1),
(8, 'Test', 'Test', '0877676545', 'annie.ann.j89@gmail.com', '', 0, 0, 0, '2018-10-04 16:55:55', '2018-10-04 16:55:55', 1),
(9, 'Sming', 'Snuan', '0894423614', 'thajchalai.s@bu.ac.th', '', 0, 0, 0, '2018-10-05 09:38:45', '2018-10-05 09:38:45', 1),
(10, 'Sming', 'Snuan', '0894423614', 'thajchalai.s@bu.ac.th', '', 0, 0, 0, '2018-10-05 09:39:25', '2018-10-05 09:39:25', 1),
(11, 'admin', 'admin', '123456', 'admin@admin.com', '$2y$12$57.rjb2B4skv.vpXe8dG6O8G87hMiXd7nRVvyDPk.GUkERHbAvZ.y', 0, 0, 0, '0000-00-00 00:00:00', '2018-11-12 11:36:49', 0),
(12, 'useredie', 'user', '123456', 'user@user.com', '$2y$12$Rx0jVI/93x096ppNO92nteNYI8Vt3fvj2tY83CVWrdikLtFw4IlQS', 0, 1, 0, '2018-11-12 13:42:30', '2018-11-12 11:43:41', 0),
(13, 'super', 'super', '12345', 'super@super.com', '$2y$12$n8Q4e6NCHUo8u6ylbFWbduea4tWHD3tKst4jVseGEnXDzCrMomfXa', 0, 2, 0, '0000-00-00 00:00:00', '2018-11-12 13:56:45', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
