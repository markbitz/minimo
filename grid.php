<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
        <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    </head>
    <body>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-8">
                        <div class="panel panel-default">
                            <div class="panel-body">Panel 1</div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-8 col-md-offset-4">
                        <div class="panel panel-default">
                            <div class="panel-body">Panel 2</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-3 col-md-offset-3">
                        <div class="panel panel-default">
                            <div class="panel-body">Panel 3</div>
                        </div>
                    </div>

                    <div class="col-xs-6 col-sm-6 col-md-3">
                        <div class="panel panel-default">
                            <div class="panel-body">Panel 4</div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12  hidden-sm col-md-6 col-md-offset-3">
                        <div class="panel panel-default">
                            <div class="panel-body">Panel 5</div>
                        </div>
                    </div>
                </div>
<footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</footer>
    </body>
</html>
