<?php
class MY_Controller extends CI_Controller {

    protected $data = array();

    function __construct()
    {
        parent::__construct();
        // set date now
        $this->dateTimeNow = date("Y-m-d H:i:s");
        $this->dateNow = date("Y-m-d");
        $this->timeNow = date("H:i:s");
    }

    protected function layout_admin($content = NULL)
    {
        $this->data['content']= (is_null($content))?'':

        $this->load->view($content, $this->data, TRUE);
        $this->load->view('layout/backend/admin_layout', $this->data);
    }
    protected  function layout_user($content = NULL)
    {
        $this->data['content']= (is_null($content))?'':

        $this->load->view($content, $this->data, TRUE);
        $this->load->view('layout/backend/user_layout', $this->data);
    }
    protected  function layout_superuser($content = NULL)
    {
        $this->data['content']= (is_null($content))?'':

        $this->load->view($content, $this->data, TRUE);
        $this->load->view('layout/backend/superuser_layout', $this->data);
    }
    protected  function layout_login($content = NULL)
    {
        $this->data['content']= (is_null($content))?'':

        $this->load->view($content, $this->data, TRUE);
        $this->load->view('layout/backend/login_layout', $this->data);
    }
    protected  function layout_frontend($content = NULL)
    {
        $this->data['content']= (is_null($content))?'':

        $this->load->view($content, $this->data, TRUE);
        $this->load->view('layout/frontend/frontend_layout', $this->data);
    }
    /*protected  function css_admin($css = NULL)
    {
        $this->data['css']= (is_null($css)) ? '' : $css;
        $this->load->view('layout/backend/admin_layout', $this->data, TRUE);
    }
    protected function js_admin($js = NULL)
    {
        $this->data['js'] = (is_null($js)) ? '' : $js;
        $this->load->view('layout/backend/admin_layout', $this->data, TRUE);
    }*/
}
