<?php
/**
 * This is the summary for a DocBlock.
 * PHP version 5
 * This is the description for a DocBlock. This text may contain
 * multiple lines and even some _markdown_.
 *
 * * Markdown style lists function too
 * * Just try this out once
 *
 * The section after the description contains the tags; which provide
 * structured meta-data concerning the given element.
 *
 * Page-Level DocBlock
 *
 * @category MyCategory
 * @package  MyPackage
 * @author   Mike van Riel <me@mikevanriel.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://example.com/my/bar Documentation of Foo.
 *
 * @return integer Indicates the number of items.
 * @since  1.0
 *
 * Page-Level DocBlock
 */

defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * This is the summary for a DocBlock.
 * PHP version 5
 * This is the description for a DocBlock. This text may contain
 * multiple lines and even some _markdown_.
 *
 * * Markdown style lists function too
 * * Just try this out once
 *
 * The section after the description contains the tags; which provide
 * structured meta-data concerning the given element.
 *
 * Page-Level DocBlock
 *
 * @category MyCategory
 * @package  MyPackage
 * @author   Mike van Riel <me@mikevanriel.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://example.com/my/bar Documentation of Foo.
 *
 * @return integer Indicates the number of items.
 * @since  1.0
 *
 * Page-Level DocBlock
 */
class Post_model extends CI_Model
{
    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @return array
     */
    public function __construct()
    {
        parent::__construct();
        $this->table = 'post';

    }
    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @return array
     */
    public function findAll()
    {
        $query = $this->db->order_by('id', 'ASC')
            ->get_where($this->table, array('deleted !=' => '1'))->result();
        return $query;
    }
    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @return array
     */
    public function findcate()
    {
        $query = $this->db->order_by('id', 'ASC')
            ->get_where($this->table, array('status !=' => '1'))->result();
        return $query;
    }
    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @return array
     */
    public function findcatenondeleteandstatus()
    {
        $query = $this->db->order_by('id', 'ASC')
            ->get_where($this->table, array('deleted !=' => '1','status !=' => '1'))
            ->result();
        return $query;
    }
    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @return array
     */
    public function findWorkshop3()
    {
        $query = $this->db->order_by('id', 'ASC')
            ->get_where($this->table, array('deleted !=' => '1','workshop =' => '3'))
            ->result();
        return $query;
    }
    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @return array
     */
    public function findWorkshop4()
    {
        $query = $this->db->order_by('id', 'ASC')
            ->get_where($this->table, array('deleted !=' => '1','workshop =' => '4'))
            ->result();
        return $query;
    }
    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @param int $id Array structure to count the elements of
     *
     * @return array
     */
    public function findById($id)
    {
        $query = $this->db
            ->get_where($this->table, array('id =' => $id))
            ->result();
        return $query;
    }
    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @return array
     */
    public function gallerythumbCount()
    {

            $this->db->select('gallery.*,COUNT(gallery_image.id_gallery) as count');
            $this->db->from('gallery');
            $this->db->group_by('id');
            $this->db->join(
                'gallery_image',
                'gallery.id = gallery_image.id_gallery', '
                left'
            );
            $query = $this->db->get()->result();
            return $query;
    }
    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @return array
     */
    public function countgallery()
    {
        $this->db->like('id', 'id_gallery');
        $this->db->from($this->table2);
            $query = $this->db->count_all_results();
            return $query;
    }
    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @param int $data Array structure to count the elements of
     *
     * @return array
     */
    public function save($data)
    {
        $date = array('updatedate' => $this->dateTimeNow,
                    'createdate' => $this->dateTimeNow);
        $this->db->insert($this->table, array_merge($data, $date));
        $id = $this->db->insert_id();
        return  $id;
    }
    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @param int $id   Array structure to count the elements of
     * @param int $data Array structure to count the elements of
     *
     * @return array
     */
    public function update($id,$data)
    {
        // update order to db
        $date = array('updatedate' => $this->dateTimeNow);
        $this->db->where('id', $id);
        $this->db->update($this->table, array_merge($data, $date));
        return true;
    }
    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @param int $id Array structure to count the elements of
     *
     * @return array
     */
    public function delete($id)
    {
        try {
            // update to db
            $this->db->set('deleted', 1);
            $this->db->set('updatedate', $this->dateTimeNow);
            $this->db->where('id', $id);
            $this->db->update($this->table);
            return true;
        } catch (Exception $e) {
            echo 'More exception: ',  $e->getMessage();
            return false;
        } // end try
    }
    public function record_count() {
       return $this->db->count_all("post");
   }

   public function fetch_countries($limit, $start) {
       $this->db->limit($limit, $start);
       $query = $this->db->get("post");

       if ($query->num_rows() > 0) {
           foreach ($query->result() as $row) {
               $data[] = $row;
           }
           return $data;
       }
       return false;
  }
}
