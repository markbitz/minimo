<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'user';
    }

    public function validate($aData)
    {

        $select = array(
            'id','firstname','lastname','email','password','tel','status',
            'level','active'
        );
        $this->db->select($select);
        $this->db->from($this->table);
        $this->db->where('email', $aData['email']);
        $this->db->limit(1);
        // Run the query
        $query = $this->db->get();


        // if found email user
        if ($query->num_rows() == 1) {
            // if there is a user, then create session data
            $row = $query->row();


            // if password pass after verify
            if (password_verify($aData['password'], $row->password)) {


                $getAccess = $this->db->select('level,status')->get_where(
                    $this->table,
                    array(
                        'deleted !=' => '1',
                        'id =' => $row->id,
                    ))->result();
                $access = array();
                foreach ($getAccess as $key => $v) {
                    $access[$key] = $v;
                }
                $sess_array = array(
                    'id' => $row->id,
                    'firstname' => $row->firstname,
                    'lastname' => $row->lastname,
                    'email' => $row->email,
                    'tel' => $row->tel,
                    'status' => $row->status,
                    'active' => $row->active,
                    'level' => $row->level
                    //'perm' => $access
                    //'user_type' => '0',
                );

                // update ipaddress to db
                /*$data = array(
                    'ipaddress' => $this->input->ip_address()
                );*/
                //$this->db->where('id', $row->id);
                //$this->db->update($this->table, $data);
                // create session
                $this->session->set_userdata('logged_in_admin', $sess_array);
                
                return true;
            } else {
                return false;
            } // end else login user
        }  // end if num rows
    }
}
