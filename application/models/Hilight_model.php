<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Hilight_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'hilight_home';
    }

    public function findAll()
    {
        $query = $this->db->order_by('id','ASC')
        ->get_where($this->table, array('deleted !=' => '1'))->result();
        return $query;
    }

    public function findWorkshop1()
    {
        $query = $this->db->order_by('id','ASC')
        ->get_where($this->table, array('deleted !=' => '1','workshop =' => '1'))->result();
        return $query;
    }

    public function findWorkshop2()
    {
        $query = $this->db->order_by('id','ASC')
        ->get_where($this->table, array('deleted !=' => '1','workshop =' => '2'))->result();
        return $query;
    }

    public function findWorkshop3()
    {
        $query = $this->db->order_by('id','ASC')
        ->get_where($this->table, array('deleted !=' => '1','workshop =' => '3'))->result();
        return $query;
    }

    public function findWorkshop4()
    {
        $query = $this->db->order_by('id','ASC')
        ->get_where($this->table, array('deleted !=' => '1','workshop =' => '4'))->result();
        return $query;
    }

    public function findById($id)
    {
        $query = $this->db->get_where($this->table,
            array('id =' => $id,'deleted !=' => '1')
            )->result();
        return $query;
    }

    public function save($data)
    {
        // save to db
        $this->db->insert($this->table, $data);
        $id = $this->db->insert_id();
        return  $id;
    }

    public function update($id_user,$data)
    {
        // update order to db
        $this->db->where('id', $id_user);
        $this->db->update($this->table, $data);
        return true;
    }

    public function delete($id)
    {
        try {
            // update to db
            $this->db->set('deleted',1);
            $this->db->set('updatedate',$this->dateTimeNow);
            $this->db->where('id',$id);
            $this->db->update($this->table);
            return true;
        } catch (Exception $e) {
            echo 'More exception: ',  $e->getMessage();
            return false;
        } // end try
    }
}
