<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Seo_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'seo';

    }

    public function findAll()
    {
        $query = $this->db->order_by('id','ASC')
        ->get_where($this->table, array('deleted !=' => '1'))->result();
        return $query;
    }

    public function findcate()
    {
        $query = $this->db->order_by('id','ASC')
        ->get_where($this->table, array('status !=' => '1'))->result();
        return $query;
    }

    public function findcatenondeleteandstatus()
    {
        $query = $this->db->order_by('id','ASC')
        ->get_where($this->table, array('deleted !=' => '1','status !=' => '1'))->result();
        return $query;
    }

    public function findWorkshop3()
    {
        $query = $this->db->order_by('id','ASC')
        ->get_where($this->table, array('deleted !=' => '1','workshop =' => '3'))->result();
        return $query;
    }

    public function findWorkshop4()
    {
        $query = $this->db->order_by('id','ASC')
        ->get_where($this->table, array('deleted !=' => '1','workshop =' => '4'))->result();
        return $query;
    }

    public function findById($id)
    {
        $query = $this->db->get_where($this->table,
            array('id =' => $id)
            )->result();
        return $query;
    }

    public function gallery_group_findById($id_gallery)
    {

        $query = $this->db->get_where($this->table2,
                        array(
                        'id_gallery =' => $id_gallery))->result();
        return $query;
    }
    public function gallery_thumb_findById($id_gallery,$id_image)
    {

        $query = $this->db->get_where($this->table2,
                                        array(

                                                'id_gallery =' => $id_gallery,
                                                'id =' => $id_image
                                            )
                                        )->result();
        return $query;
    }
    public function gallerythumbCount()
    {

            $this->db->select('gallery.*,COUNT(gallery_image.id_gallery) as count');
            $this->db->from('gallery');
            $this->db->group_by('id');
            $this->db->join('gallery_image', 'gallery.id = gallery_image.id_gallery','left');
            $query = $this->db->get( )->result();
            return $query;
    }
    public function countgallery()
    {

        $this->db->like('id', 'id_gallery');
        $this->db->from($this->table2);
            $query = $this->db->count_all_results();

            return $query;
    }

    public function save($data)
    {
        $date = array('updatedate' => $this->dateTimeNow,
                    'createdate' => $this->dateTimeNow);
        $this->db->insert($this->table,array_merge($data,$date));
        $id = $this->db->insert_id();
        return  $id;
    }
    public function save_gallery($data)
    {
        // save to db
        $this->db->insert($this->table2, $data);
        $id = $this->db->insert_id();
        return  $id;
    }

    public function update($id,$data)
    {
        // update order to db
        $date = array('updatedate' => $this->dateTimeNow);
        $this->db->where('id', $id);
        $this->db->update($this->table, array_merge($data,$date));
        return true;
    }
    public function update_thumb($id_gallery,$data)
    {
        // update order to db
        $this->db->where('id', $id_gallery);
        $this->db->update($this->table, $data);
        return true;
    }

    public function delete($id)
    {
        try {
            // update to db
            $this->db->set('deleted',1);
            $this->db->set('updatedate',$this->dateTimeNow);
            $this->db->where('id',$id);
            $this->db->update($this->table);
            return true;
        } catch (Exception $e) {
            echo 'More exception: ',  $e->getMessage();
            return false;
        } // end try
    }
}
