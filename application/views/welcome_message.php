<!DOCTYPE html>
<!--
Item Name: Elisyam - Web App & Admin Dashboard Template
Version: 1.5
Author: SAEROX

** A license must be purchased in order to legally use this template for your project **
-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Elisyam - Locked Session</title>
        <meta name="description" content="Elisyam is a Web App and Admin Dashboard Template built with Bootstrap 4">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Google Fonts -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
        <script>
          WebFont.load({
            google: {"families":["Montserrat:400,500,600,700","Noto+Sans:400,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="assets/image/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="assets/image/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="assets/image/favicon-16x16.png">
        <!-- Stylesheet -->
        <?php

            if(isset($css))
            {
                foreach($css as $key => $value)
                {
                    echo link_tag(
                        array(
                            'href' => base_url().$value,
                            'rel' => 'stylesheet',
                            'type' => 'text/css'
                        )
                );
                }
            }
        ?>
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    </head>
    <body class="bg-fixed-01">
        <!-- Begin Preloader -->
        <div id="preloader">
            <div class="canvas">
                <img src="assets/image/logo.png" alt="logo" class="loader-logo">
                <div class="spinner"></div>
            </div>
        </div>
        <!-- End Preloader -->
        <!-- Begin Section -->
        <div class="container-fluid h-100 overflow-y">
            <div class="row flex-row h-100">
                <div class="col-12 my-auto">
                    <div class="lock-form mx-auto">
                        <div class="photo-profil">

                            <img src="assets/image/avatar/avatar-01.jpg" alt="..." class="img-fluid rounded-circle">
                        </div>
                        <h3>Welcome</h3>

                        <div class="button text-center">
                            <a href="login" class="btn btn-lg btn-gradient-01">
                                Login
                            </a>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End Container -->
        </div>
        <!-- End Section -->
        <!-- Begin Vendor Js -->
        <?php

            if(isset($js)){
                foreach($js as $key => $value)
                {
                    echo script_tag(
                        array(
                            ' src' => base_url().$value,
                            'rel' => 'stylesheet',
                            'type' => 'text/javascript'
                        )
                    );

                }
            }
        ?>
    </body>
</html>
