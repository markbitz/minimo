<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Working</title>
        <meta name="description" content="Elisyam is a Web App and Admin Dashboard Template built with Bootstrap 4">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Google Fonts -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
        <script>
          WebFont.load({
            google: {"families":["Montserrat:400,500,600,700","Noto+Sans:400,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="<?php base_url('assets/image/apple-touch-icon.png');?>">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php base_url('assets/image/favicon-32x32.png');?>">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php base_url('assets/image/favicon-16x16.png');?>">
        <!-- Stylesheet -->
        <?php        
            if(isset($css))
            {
                foreach($css as $key => $value)
                {
                    echo link_tag(
                        array(
                            'href' => base_url().$value,
                            'rel' => 'stylesheet',
                            'type' => 'text/css'
                        )
                );
                }
            }
        ?>

        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    </head>
    <body id="page-top">
        <div class="page">
            <!-- Begin Header -->
            <header class="header">
                <nav class="navbar fixed-top">
                    <!-- Begin Search Box-->
                    <div class="search-box">
                        <button class="dismiss"><i class="ion-close-round"></i></button>
                        <form id="searchForm" action="#" role="search">
                            <input type="search" placeholder="Search something ..." class="form-control">
                        </form>
                    </div>
                    <!-- End Search Box-->
                    <!-- Begin Topbar -->
                    <div class="navbar-holder d-flex align-items-center align-middle justify-content-between">
                        <!-- Begin Logo -->
                        <div class="navbar-header">
                            <a href="db-default.html" class="navbar-brand">
                                <div class="brand-image brand-big">
                                    <img src="<?php echo base_url('assets/image/logo-big.png');?>" alt="logo" class="logo-big">
                                </div>
                                <div class="brand-image brand-small">
                                    <img src="<?php echo base_url('assets/image/logo.png');?>" alt="logo" class="logo-small">
                                </div>
                            </a>
                            <!-- Toggle Button -->
                            <a id="toggle-btn" href="#" class="menu-btn active">
                                <span></span>
                                <span></span>
                                <span></span>
                            </a>
                            <!-- End Toggle -->
                        </div>
                        <!-- End Logo -->
                        <!-- Begin Navbar Menu -->
                        <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center pull-right">
                            <!-- Search -->

                            <!-- End Search -->
                            <!-- Begin Notifications -->

                            <!-- End Notifications -->
                            <!-- User -->
                            <li class="nav-item dropdown"><a id="user" rel="nofollow" data-target="#" href="#"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link">
                                <img src="<?php echo base_url('assets/image/avatar/avatar-01.jpg')?>" alt="..." class="avatar rounded-circle"></a>
                                <ul aria-labelledby="user" class="user-size dropdown-menu">
                                    <li class="welcome">
                                        <a href="#" class="edit-profil"><i class="la la-gear"></i></a>
                                        <img src="<?php echo base_url('assets/image/avatar/avatar-01.jpg');?>" alt="..." class="rounded-circle">
                                    </li>
                                    <li>
                                        <a href="<?php

                                        $lv = $_SESSION['logged_in_admin']['level'];
                                        switch ($lv) {
                                            case '0':
                                                echo base_url('backend/main');
                                                break;
                                            case '1':
                                                echo base_url('backend/user');
                                                break;
                                            case '2':
                                                echo base_url('backend/superuser');
                                                break;
                                            default:
                                                echo 'User Incorrect Contact Administartor!!';
                                                break;
                                        } // end switch

                                         ?>" class="dropdown-item">
                                            Profile
                                        </a>
                                    </li>



                                    <li><a rel="nofollow" href="<?php echo base_url('backend/login/logout'); ?>" class="dropdown-item logout text-center"><i class="ti-power-off"></i></a></li>
                                </ul>
                            </li>
                            <!-- End User -->
                            <!-- Begin Quick Actions -->

                            <!-- End Quick Actions -->
                        </ul>
                        <!-- End Navbar Menu -->
                    </div>
                    <!-- End Topbar -->
                </nav>
            </header>




            <?php include 'menu.php' ?>
            <div class="content-inner">
                <div class="container-fluid">
    <!-- Begin Page Header-->

    <!-- End Page Header -->
    <!-- Begin Row -->
                    <div class="row flex-row">
                        <div class="col-xl-12 col-12">
                            <div class="widget has-shadow">
                                <div class="widget-body">
                                    <p class="text-primary mt-2 mb-2"><?php echo $content;?></p>
                                </div>
                            </div>
                        </div>
                    </div>
    <!-- End Row -->
                </div>


<!-- End Content -->
            </div>
<!-- End Page Content -->
        </div>
<!-- Begin Vendor Js -->

                <?php

                    if(isset($js)){
                        foreach($js as $key => $value)
                        {
                            echo script_tag(
                                array(
                                    ' src' => base_url().$value,
                                    'rel' => 'stylesheet',
                                    'type' => 'text/javascript'
                                )
                            );

                        }
                    }
                ?>

        <!--        <script src="assets/js/jquery.min.js"></script>
                <script src="assets/js/core.min.js"></script>


                <script src="assets/plugin/js/nicescroll/nicescroll.min.js"></script>
                <script src="assets/plugin/js/app/app.min.js"></script> -->



<!-- End Vendor Js -->

</body>
</html>
