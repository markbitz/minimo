<div class="page-content d-flex align-items-stretch">
    <div class="default-sidebar">
        <!-- Begin Side Navbar -->
        <nav class="side-navbar box-scroll sidebar-scroll">
            <!-- Begin Main Navigation -->
            <span class="heading">Profile</span>
            <ul class="list-unstyled">
                <?php
                $lv = $_SESSION['logged_in_admin']['level'];
                ?>
                <li><a href="<?php echo base_url('backend/dashboard'); ?>">
                    <i class="la la-spinner"></i><span>Dashboard</span></a></li>
                <li><a id="<?php    if($lv!=0)
                                    {
                                        echo "hide";
                                    }
                            ?>"href="<?php echo base_url('backend'); ?>">
                    <i class="la la-spinner"></i><span>User</span></a>
                </li>
                <li><a href="<?php echo base_url('backend/gallery'); ?>">
                    <i class="la la-spinner"></i><span>Gallery</span></a>
                </li>
                <li><a href="<?php echo base_url('backend/categories'); ?>">
                    <i class="la la-spinner"></i><span>Categories</span></a>
                </li>
                <li><a href="<?php echo base_url('backend/seo/add'); ?>">
                    <i class="la la-spinner"></i><span>SEO</span></a>
                </li>
                <li><a href="<?php echo base_url('backend/post'); ?>">
                    <i class="la la-spinner"></i><span>Post</span></a>
                </li>
                <ul class="list-unstyled">
                    <li>
                        <a href="<?php echo base_url('backend/login/logout'); ?>">
                        <i class="la la-angle-left"></i><span>Logout</span></a></li>
                </ul>
            </ul>
            <!-- End Main Navigation -->
        </nav>
        <!-- End Side Navbar -->
    </div>
