<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <?php
            if (isset($seo)) {
                foreach ($seo as $key => $value) {
                    $url = $seo[$key]=$value->url;
                    $type = $seo[$key]=$value->type;
                    $title = $seo[$key]=$value->title;
                    $description = $seo[$key]=$value->description;
                    $image = $seo[$key]=$value->image;
                    $site_name = $seo[$key]=$value->site_name;
         ?>
        <meta property='url' content='<?php echo $url; ?>'>
        <meta property='type' content='<?php echo $type ; ?>'>
        <meta property='title' content='<?php echo $title; ?>'>
        <meta property='description' content='<?php echo $description; ?>'>
        <meta property='image' content='<?php echo $image; ?>'>
        <meta property='site_name' content='<?php echo $site_name; ?>'>
        <?php
                }
            }
         ?>
        <title>Mini</title>
        <?php
            if(isset($css))
            {
                foreach($css as $key => $value)
                {
                    echo link_tag(
                        array(
                            'href' => base_url().$value,
                            'rel' => 'stylesheet',
                            'type' => 'text/css'
                        )
                );
                }
            }
        ?>
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    </head>
    <body>
        <header>
            <nav id="custom-bootstrap-menu" class="navbar navbar-default mynav " >
                <div class="container">
                    <div class="row ">
                        <div class="col-xs-12 col-md-2 col-md-offset-1">
                            <div class="navbar-header ">
                                <button type="button" class=" navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#navbar-collapse" aria-expanded="false">
                                  <span class="sr-only">Toggle navigation</span>
                                  <span class="icon-bar "></span>
                                  <span class="icon-bar"></span>
                                  <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand fontslogo " href="main" >minimø</a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-8 ">
                            <div class="collapse navbar-collapse" id="navbar-collapse">
                            <ul class="nav navbar-nav  nav-right ">
                                <?php
                                foreach ($data_categories as $key => $value) {
                                ?>
                                <li><a class="nav-link " href="main" ><?php echo $value->name; ?></a></li>
                                <?php
                                }
                                 ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </header>
                <?php echo $content;?>
        <footer class="footer">
            <div class="container text-center">
                <div class="row">
                    <div class="col-xs-12  col-md-12">
                        <span>Copyright © 1962 - 2018. All Rights Reserved.</span>
                    </div>
                </div>
            </div>
        </footer>
    </body>
        <?php
            if(isset($js)){
                foreach($js as $key => $value)
                {
                    echo script_tag(
                        array(
                            ' src' => base_url().$value,
                            'rel' => 'stylesheet',
                            'type' => 'text/javascript'
                        )
                    );

                }
            }
        ?>
        <script src="http://propeller.in/components/global/js/global.js"></script>
        <script type="text/javascript" src="http://propeller.in/components/textfield/js/textfield.js"></script>
</html>
