<?php
    if($this->session->flashdata('msg-success')){
        $this->msg->success();
    }elseif ($this->session->flashdata('msg-warning')) {
        $this->msg->warning();
    }
    if ($seo != NULL) {
        foreach ($seo as $key => $value) {
            $id = $value->id;
            $url = $value->url;
            $type = $value->type;
            $title = $value->title;
            $description = $value->description;
            $image = $value->image;
            $site_name = $value->site_name;
        }
    } else {
        $id = NULL;
        $url = NULL;
        $type = NULL;
        $title = NULL;
        $description = NULL;
        $image = NULL;
        $site_name = NULL;
    }
 ?>
<div class="container-fluid">
    <!-- Begin Page Header-->
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">SEO</h2>
                <div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="db-default.html"><i class="ti ti-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item active">SEO</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- End Page Header -->
    <div class="row flex-row">

        <div class="col-xl-9">
            <div class="widget has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h4>SEO</h4>
                </div>
                <div class="widget-body">
                    <?php
                    echo form_open('backend/seo/save');
                    $hidden = array('id' => $id,);
                    echo form_hidden($hidden);
                    ?>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <?php
                            $attributes = array(
                                'class' => 'col-lg-2 form-control-label d-flex justify-content-lg-end'
                                );
                            echo form_label('Url', 'url',$attributes);?>
                            <div class="col-lg-6">
                                <?php
                                $url_option = array(
                                        'type'  => 'text',
                                        'name'  => 'url',
                                        'id'    => 'url',
                                        'value' => set_value('url',$url),
                                        'class' => 'form-control',
                                        //'required' => 'true'
                                    );
                                echo form_input($url_option);
                                 ?>
                            </div>
                            <span class="error">* <?php echo form_error('url');?></span>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <?php
                            $attributes = array(
                                'class' => 'col-lg-2 form-control-label d-flex justify-content-lg-end'
                                );
                            echo form_label('Type', 'type',$attributes);?>
                            <div class="col-lg-6">
                                <?php
                                $type_option = array(
                                        'type'  => 'text',
                                        'name'  => 'type',
                                        'id'    => 'type',
                                        'value' => set_value('type',$type),
                                        'class' => 'form-control',
                                        //'required' => 'true'
                                    );
                                    echo form_input($type_option);

                                 ?>
                            </div>
                            <span class="error">* <?php echo form_error('type');?></span>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <?php
                            $attributes = array(
                                'class' => 'col-lg-2 form-control-label d-flex justify-content-lg-end'
                                );
                            echo form_label('Title', 'title',$attributes);?>
                            <div class="col-lg-6">
                                <?php
                                $title_option = array(
                                        'type'  => 'text',
                                        'name'  => 'title',
                                        'id'    => 'title',
                                        'value' => set_value('title',$title),
                                        'class' => 'form-control',
                                        //'required' => 'true'
                                    );
                                    echo form_input($title_option);
                                 ?>
                            </div>
                            <span class="error">* <?php echo form_error('title');?></span>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <?php
                            $attributes = array(
                                'class' => 'col-lg-2 form-control-label d-flex justify-content-lg-end'
                                );
                            echo form_label('Description', 'description',$attributes);?>
                            <div class="col-lg-6">
                                <?php
                                $description_option = array(
                                        'type'  => 'text',
                                        'name'  => 'description',
                                        'id'    => 'description',
                                        'value' => set_value('description',$description),
                                        'class' => 'form-control',
                                        //'required' => 'true'
                                    );
                                    echo form_input($description_option);
                                 ?>
                            </div>
                            <span class="error">* <?php echo form_error('description');?></span>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <?php
                            $attributes = array(
                                'class' => 'col-lg-2 form-control-label d-flex justify-content-lg-end'
                                );
                            echo form_label('Image', 'image',$attributes);?>
                            <div class="col-lg-6">
                                <?php

                                $image_option = array(
                                        'type'  => 'text',
                                        'name'  => 'image',
                                        'id'    => 'image',
                                        'value' => set_value('image',$image),
                                        'class' => 'form-control',
                                        //'required' => 'true'
                                    );
                                    echo form_input($image_option);

                                 ?>
                            </div>
                            <span class="error">* <?php echo form_error('image');?></span>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <?php
                            $attributes = array(
                                'class' => 'col-lg-2 form-control-label d-flex justify-content-lg-end'
                                );
                            echo form_label('Site name', 'site_name',$attributes);?>
                            <div class="col-lg-6">
                                <?php
                                    $site_name_option = array(
                                        'type'  => 'text',
                                        'name'  => 'site_name',
                                        'id'    => 'site_name',
                                        'value' => set_value('site_name',$site_name),
                                        'class' => 'form-control',
                                        //'required' => 'true'
                                    );
                                    echo form_input($site_name_option);

                                 ?>
                            </div>
                            <span class="error">* <?php echo form_error('site_name');?></span>
                        </div>
                        <div class="em-separator separator-dashed"></div>
                            <div class="text-right">
                                <?php
                                if ($seo == NULL) {
                                    $submit = array(
                                        'name'          => 'button',
                                        'id'            => 'button',
                                        'value'         => 'true',
                                        'type'          => 'submit',
                                        'class'          => 'btn btn-gradient-01',
                                        'content'       => 'บันทึก'
                                        );
                                        echo form_button($submit);
                                        $reset = array(
                                            'name'          => 'button',
                                            'id'            => 'button',
                                            'value'         => 'true',
                                            'type'          => 'reset',
                                            'class'          => 'btn btn-gradient-01',
                                            'content'       => 'Reset'
                                            );
                                            echo form_button($reset);
                                } else {
                                    $submit = array(
                                        'name'          => 'button',
                                        'id'            => 'button',
                                        'value'         => 'true',
                                        'type'          => 'submit',
                                        'class'          => 'btn btn-gradient-01',
                                        'content'       => 'บันทึก'
                                        );
                                        echo form_button($submit);
                                }

                                ?>

                            </div>
                            <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    <!-- End Row -->
</div>
