<?php
    if($this->session->flashdata('msg-success')){
        $this->msg->success();
    }elseif ($this->session->flashdata('msg-warning')) {
        $this->msg->warning();
    }
 ?>
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">SEO</h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="db-all.html"><i class="ti ti-home"></i></a></li>
                    <li class="breadcrumb-item active">SEO</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="widget has-shadow">
    <div class="widget-header bordered no-actions d-flex align-items-center">
        <h4>SEO</h4>
    </div>
    <div class="widget-body">
        <div class="table-responsive">

            <table id="sorting-table" class="table mb-0">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Url</th>
                        <th>type</th>
                        <th>Ttitle</th>
                        <th>Description</th>
                        <th>Image</th>
                        <th>site_name</th>
                        <th>favicon</th>
                        <th>Createdate</th>
                        <th>Updatedate</th>
                        <th>Edi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php


                    $count=1;
                    foreach ($seo as $i => $row)
                    {
                    ?>
                    <tr>
                        <td><?php echo $count; ?></td>
          				<td><?php echo $row->url; ?></td>
                        <td><?php echo $row->type; ?></td>
                        <td><?php echo $row->title; ?></td>
                        <td><?php echo $row->description; ?></td>
                        <td><?php echo $row->image; ?></td>
                        <td><?php echo $row->site_name; ?></td>
          				<td><?php echo $row->favicon; ?></td>
                        <td><?php echo $row->createdate; ?></td>
          				<td><?php echo $row->updatedate; ?></td>


                        <td class="td-actions">
                            <a href="<?php echo base_url('backend/seo/edit/').$row->id; ?>"><i class="la la-edit edit"></i></a>
                            <a href="<?php echo base_url('backend/seo/delete/').$row->id; ?>"><i class="la la-close delete"></i></a>
                        </td>
                        </tr>
                        <?php
                            $count++;
                        } // end foreach
                        ?>
                </tbody>
        </table>
        <div class="text-right">
            <a href="<?php echo base_url('backend/seo/add'); ?> " class="btn btn-secondary mr-1 mb-2" role="button">บันทึก</a>

        </div>
        </div>
            </div>
