<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
        <?php
            if(isset($css))
            {
                foreach($css as $key => $value)
                {
                    echo link_tag(
                        array(
                            'href' => base_url().$value,
                            'rel' => 'stylesheet',
                            'type' => 'text/css'
                        )
                );
                }
            }
        ?>
    </head>
    <body>
        <form class="form-horizontal" method="post" action="main/insert">


            <div class="form-group row d-flex align-items-center mb-5">
                <label class="col-lg-3 form-control-label">Email</label>
                <div class="col-lg-9">
                    <input type="email" class="form-control" name="email">
                </div>
            </div>
            <div class="form-group row d-flex align-items-center mb-5">
                <label class="col-lg-3 form-control-label">Password</label>
                <div class="col-lg-9">
                    <input type="password" class="form-control" name="password">
                </div>
            </div>
            <div class="col-12 text-right">
                <button type="submit" class="btn btn-primary btn-square mr-1 mb-2 ">Login</button>
            </div>
        </form>
        <?php
            if(isset($js)){
                foreach($js as $key => $value)
                {
                    echo link_tag(
                        array(
                            'src' => base_url().$value,
                            'rel' => 'stylesheet',
                            'type' => 'text/javascript'
                        )
                    );
                }
            }
        ?>
    </body>
</html>
