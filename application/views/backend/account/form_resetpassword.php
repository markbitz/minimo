<?php
    if($this->session->flashdata('msg-success')){
        $this->msg->success();
        }
        foreach ($account as $key => $value) {
            $id = $value->id;
            $email = $value->email;
        }

 ?>
<div class="container-fluid">
    <!-- Begin Page Header-->
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">Account</h2>
                <div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="db-default.html"><i class="ti ti-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item active">Account</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- End Page Header -->
    <div class="row flex-row">

        <div class="col-xl-10">
            <div class="widget has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h4>Account</h4>
                </div>
                <div class="widget-body">
                    <?php
                    echo form_open('backend/account/reset_password');
                    $hidden = array('id' => $id,);
                    echo form_hidden($hidden);
                    ?>
                    <div class="form-group row d-flex align-items-center mb-5">
                        <?php
                        $attributes = array(
                            'class' => 'col-lg-3 form-control-label d-flex justify-content-lg-end'
                            );
                        echo form_label($email, 'name',$attributes);?>
                    </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <?php
                            $attributes = array(
                                'class' => 'col-lg-3 form-control-label d-flex justify-content-lg-end'
                                );
                            echo form_label('Password', 'name',$attributes);?>
                            <div class="col-lg-6">
                                <?php

                                $password_option = array(
                                        'type'  => 'password',
                                        'name'  => 'password',
                                        'id'    => 'password',
                                        //'value' => set_value('password',$password),
                                        'class' => 'form-control',
                                        //'required' => 'true'
                                    );
                                    echo form_input($password_option);

                                 ?>
                            </div>
                            <span class="error">* <?php echo form_error('password');?></span>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <?php
                            $attributes = array(
                                'class' => 'col-lg-3 form-control-label d-flex justify-content-lg-end'
                                );
                            echo form_label('Password Confirm', 'name',$attributes);?>
                            <div class="col-lg-6">
                                <?php
                                    $passwordcon_option = array(
                                        'type'  => 'password',
                                        'name'  => 'passwordcon',
                                        'id'    => 'passwordcon',
                                        //'value' => set_value('passwordcon',$passwordcon),
                                        'class' => 'form-control',
                                        //'required' => 'true'
                                    );
                                    echo form_input($passwordcon_option);

                                 ?>
                            </div>
                            <span class="error">* <?php echo form_error('passwordcon');?></span>
                        </div>
                        <div class="em-separator separator-dashed"></div>
                            <div class="text-right">
                                <?php
                                if ($account == NULL) {
                                    $submit = array(
                                        'name'          => 'button',
                                        'id'            => 'button',
                                        'value'         => 'true',
                                        'type'          => 'submit',
                                        'class'          => 'btn btn-gradient-01',
                                        'content'       => 'บันทึก'
                                        );
                                        echo form_button($submit);
                                        $reset = array(
                                            'name'          => 'button',
                                            'id'            => 'button',
                                            'value'         => 'true',
                                            'type'          => 'reset',
                                            'class'          => 'btn btn-gradient-01',
                                            'content'       => 'Reset'
                                            );
                                            echo form_button($reset);
                                } else {
                                    $submit = array(
                                        'name'          => 'button',
                                        'id'            => 'button',
                                        'value'         => 'true',
                                        'type'          => 'submit',
                                        'class'          => 'btn btn-gradient-01',
                                        'content'       => 'บันทึก'
                                        );
                                        echo form_button($submit);
                                }

                                ?>

                            </div>
                            <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    <!-- End Row -->
</div>
