<?php
    if($this->session->flashdata('msg-success')){
        $this->msg->success();
    }
 ?>
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">User</h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="db-all.html"><i class="ti ti-home"></i></a></li>
                    <li class="breadcrumb-item active">User</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="widget has-shadow">
    <div class="widget-header bordered no-actions d-flex align-items-center">
        <h4>User</h4>
    </div>
    <div class="widget-body">
        <div class="table-responsive">
            <table id="sorting-table" class="table mb-0">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Firstname</th>
                        <th>Lastname</th>
                        <th>Tel</th>
                        <th>Email</th>
                        <th>Edit</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $count=1;
                    foreach ($data as $i => $row)
                    {
                    ?>
                    <tr>
                        <td><?php echo $count; ?></td>
                        <td><?php echo $row->firstname; ?></td>
                        <td><?php echo $row->lastname; ?></td>
                        <td><?php echo $row->tel; ?></td>
                        <td><?php echo $row->email; ?></td>

                        <td class="td-actions">
                            <a href="backend/edit/<?php echo $row->id; ?>"><i class="la la-edit edit"></i></a>
                            <a href="backend/reset_password/<?php echo $row->id; ?>"><i class="la la-minus-circle edit"></i></a>
                            <a href="backend/delete/<?php echo $row->id; ?>"><i class="la la-close delete"></i></a>
                        </td>
                        </tr>
                        <?php
                            $count++;
                        } // end foreach
                        ?>
                </tbody>
        </table>
        <div class="text-right">
            <a href="backend/add" class="btn btn-secondary mr-1 mb-2" role="button">New Account</a>
        </div>
        </div>
            </div>
