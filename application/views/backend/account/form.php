<?php
    if($this->session->flashdata('msg-success')){
        $this->msg->success();
        }
    if ($account != NULL) {
        foreach ($account as $key => $value) {
            $id = $value->id;
            $firstname = $value->firstname;
            $lastname = $value->lastname;
            $tel = $value->tel;
            $email = $value->email;
            $level = $value->level;
        }
    } else {
        $id = NULL;
        $firstname = NULL;
        $lastname = NULL;
        $tel = NULL;
        $email = NULL;
        $level = NULL;
    }
 ?>
<div class="container-fluid">
    <!-- Begin Page Header-->
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">Account</h2>
                <div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="db-default.html"><i class="ti ti-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item active">Account</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- End Page Header -->
    <div class="row flex-row">

        <div class="col-xl-10">
            <div class="widget has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h4>Account</h4>
                </div>
                <div class="widget-body">
                    <?php
                    echo form_open('backend/account/save');
                    $hidden = array('id' => $id,);
                    echo form_hidden($hidden);
                    ?>
                    <div class="form-group row d-flex align-items-center mb-5">
                        <?php
                        $attributes = array(
                            'class' => 'col-lg-3 form-control-label d-flex justify-content-lg-end'
                            );
                        echo form_label('Email', 'email',$attributes);?>
                        <div class="col-lg-6">
                            <?php
                            $email_option = array(
                                    'type'  => 'email',
                                    'name'  => 'email',
                                    'id'    => 'email',
                                    'value' => set_value('email',$email),
                                    'class' => 'form-control',
                                    //'required' => 'true'
                                );
                                echo form_input($email_option);
                             ?>
                        </div>
                        <span class="error">* <?php echo form_error('email');?></span>
                    </div>
                    <?php if ($id == NULL) {
                        ?>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <?php
                            $attributes = array(
                                'class' => 'col-lg-3 form-control-label d-flex justify-content-lg-end'
                                );
                            echo form_label('Password', 'password',$attributes);?>
                            <div class="col-lg-6">
                                <?php

                                $password_option = array(
                                        'type'  => 'password',
                                        'name'  => 'password',
                                        'id'    => 'password',
                                        //'value' => set_value('password',$password),
                                        'class' => 'form-control',
                                        //'required' => 'true'
                                    );
                                    echo form_input($password_option);

                                 ?>
                            </div>
                            <span class="error">* <?php echo form_error('password');?></span>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <?php
                            $attributes = array(
                                'class' => 'col-lg-3 form-control-label d-flex justify-content-lg-end'
                                );
                            echo form_label('Password Confirm', 'passwordcon',$attributes);?>
                            <div class="col-lg-6">
                                <?php
                                    $passwordcon_option = array(
                                        'type'  => 'password',
                                        'name'  => 'passwordcon',
                                        'id'    => 'passwordcon',
                                        //'value' => set_value('passwordcon',$passwordcon),
                                        'class' => 'form-control',
                                        //'required' => 'true'
                                    );
                                    echo form_input($passwordcon_option);

                                 ?>
                            </div>
                            <span class="error">* <?php echo form_error('passwordcon');?></span>
                        </div>

                        <?php
                    }
                     ?>


                        <div class="form-group row d-flex align-items-center mb-5">
                            <?php
                            $attributes = array(
                                'class' => 'col-lg-3 form-control-label d-flex justify-content-lg-end'
                                );
                            echo form_label('Firstname', 'firstname',$attributes);?>
                            <div class="col-lg-6">
                                <?php
                                $firstname_option = array(
                                        'type'  => 'text',
                                        'name'  => 'firstname',
                                        'id'    => 'firstname',
                                        'value' => set_value('firstname',$firstname),
                                        'class' => 'form-control',
                                        //'required' => 'true'
                                    );
                                echo form_input($firstname_option);
                                 ?>
                            </div>
                            <span class="error">* <?php echo form_error('firstname');?></span>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <?php
                            $attributes = array(
                                'class' => 'col-lg-3 form-control-label d-flex justify-content-lg-end'
                                );
                            echo form_label('Lastname', 'lastname',$attributes);?>
                            <div class="col-lg-6">
                                <?php
                                $lastname_option = array(
                                        'type'  => 'text',
                                        'name'  => 'lastname',
                                        'id'    => 'lastname',
                                        'value' => set_value('lastname',$lastname),
                                        'class' => 'form-control',
                                        //'required' => 'true'
                                    );
                                    echo form_input($lastname_option);

                                 ?>
                            </div>
                            <span class="error">* <?php echo form_error('lastname');?></span>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <?php
                            $attributes = array(
                                'class' => 'col-lg-3 form-control-label d-flex justify-content-lg-end'
                                );
                            echo form_label('Tel', 'tel',$attributes);?>
                            <div class="col-lg-6">
                                <?php
                                $tel_option = array(
                                        'type'  => 'text',
                                        'name'  => 'tel',
                                        'id'    => 'tel',
                                        'value' => set_value('tel',$tel),
                                        'class' => 'form-control',
                                        //'required' => 'true'
                                    );
                                    echo form_input($tel_option);
                                 ?>
                            </div>
                            <span class="error">* <?php echo form_error('tel');?></span>
                        </div>
                        <div class="form-group row mb-5">
                            <?php
                            $attributes = array(
                                'class' => 'col-lg-3 form-control-label d-flex justify-content-lg-end'
                                );
                            echo form_label('Stetus', 'level',$attributes);?>
                            <div class="col-lg-6 select mb-3">
                                <?php
                                $options = array(
                                    '0'   => 'Admin',
                                    '1'   => 'User',
                                    '2'   => 'Superuser'
                                );
                                if ($account == NULL) {
                                    echo form_dropdown('level', $options);
                                } else {
                                    echo form_dropdown('level', $options,$level);
                                }
                                 ?>
                            </div>
                        </div>
                        <div class="em-separator separator-dashed"></div>
                            <div class="text-right">
                                <?php
                                if ($account == NULL) {
                                    $submit = array(
                                        'name'          => 'button',
                                        'id'            => 'button',
                                        'value'         => 'true',
                                        'type'          => 'submit',
                                        'class'          => 'btn btn-gradient-01',
                                        'content'       => 'บันทึก'
                                        );
                                        echo form_button($submit);
                                        $reset = array(
                                            'name'          => 'button',
                                            'id'            => 'button',
                                            'value'         => 'true',
                                            'type'          => 'reset',
                                            'class'          => 'btn btn-gradient-01',
                                            'content'       => 'Reset'
                                            );
                                            echo form_button($reset);
                                } else {
                                    $submit = array(
                                        'name'          => 'button',
                                        'id'            => 'button',
                                        'value'         => 'true',
                                        'type'          => 'submit',
                                        'class'          => 'btn btn-gradient-01',
                                        'content'       => 'บันทึก'
                                        );
                                        echo form_button($submit);
                                }

                                ?>

                            </div>
                            <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    <!-- End Row -->
</div>
