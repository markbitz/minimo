<div class="container-fluid">
    <!-- Begin Page Header-->
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">Profile</h2>
                <div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="db-default.html"><i class="ti ti-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item active">Profile</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- End Page Header -->
    <div class="row flex-row">

        <div class="col-xl-9">
            <div class="widget has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h4>Update Profile</h4>
                </div>
                <div class="widget-body">

                    <form class="form-horizontal" method="post" action="
                    <?php
                    
                    $lv = $_SESSION['logged_in_admin']['level'];
                    switch ($lv) {
                        case '0':
                            echo base_url('backend/main/create_gallery');
                            break;
                        case '1':
                            echo base_url('backend/user/create_gallery');
                            break;
                        case '2':
                            echo base_url('backend/superuser/create_gallery');
                            break;
                        default:
                            echo 'User Incorrect Contact Administartor!!';
                            break;
                    } // end switch
                    ?>">
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label d-flex justify-content-lg-end">Create Gallery</label>
                            <div class="col-lg-6">

                                <input type="Text" class="form-control"  name="namegallery">
                            </div>
                        </div>





                    <div class="em-separator separator-dashed"></div>
                    <div class="text-right">
                        <button class="btn btn-gradient-01" type="submit">Create</button>

                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Row -->
</div>
