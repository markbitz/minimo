

<div class="container-fluid">
    <!-- Begin Page Header-->
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">Gallery</h2>
                <div>
                    <ul class="breadcrumb">

                        <li class="breadcrumb-item active">Gallery</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3">
        <div class="widget has-shadow">


            <div class="widget-body">
                <a href="
                <?php
                $lv = $_SESSION['logged_in_admin']['level'];
                switch ($lv) {
                    case '0':
                        echo base_url('backend/main/gallery/create_gallery');
                        break;
                    case '1':
                        echo base_url('backend/user/gallery/create_gallery');
                        break;
                    case '2':
                        echo base_url('backend/superuser/gallery/create_gallery');
                        break;
                    default:
                        echo 'User Incorrect Contact Administartor!!';
                        break;
                } // end switch


                ?>


                " class="btn btn-primary mr-1 mb-2">Create Gallery</a>

            </div>

        </div>
    </div>

    <!-- End Page Header -->
    <div class="row flex-row">
        <div class="row">
            <?php
            foreach ($data as $key => $value) {
             ?>

 <div class="col-md-4">
     <div class="widget has-shadow col-md-12">
         <div class="thumbnail">
             <br>
             <div class="text-right">
                 <p class="btn btn-danger ripple mr-1 mb-2"><?php echo $value->count; ?></p>
             </div>
             <a href="<?php
             $lv = $_SESSION['logged_in_admin']['level'];
             switch ($lv) {
                 case '0':
                     echo base_url('backend/main/gallery/group/').$value->id;
                     break;
                 case '1':
                     echo base_url('backend/user/gallery/group/').$value->id;
                     break;
                 case '2':
                     echo base_url('backend/superuser/gallery/group/').$value->id;
                     break;
                 default:
                     echo 'User Incorrect Contact Administartor!!';
                     break;
             } // end switch

             //echo base_url('backend/user/gallery/group/').$value->id;
             ?>">
                 <img src="/mini/uploads/gallery/thumb/<?php echo $value->tump_image  ?>" alt="Lights" style="width:100%">
                 <div class="caption">
                     <div class="col-md-12">
                         <br>
                         <p><?php echo $value->gallery_name; ?></p>
                     </div>
                 </div>
             </a>
         </div>

     </div>
 </div>
        <?php
        }
        ?>


</div>

    </div>
    <!-- End Row -->
</div>
