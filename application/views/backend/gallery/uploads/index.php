<div class="container-fluid">
    <!-- Begin Page Header-->
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">Profile</h2>
                <div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="db-default.html"><i class="ti ti-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item active">Profile</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- End Page Header -->
    <div class="row flex-row">


        <div class="col-xl-9">
            <div class="widget has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h4>Update Profile</h4>
                </div>
                <div class="widget-body">


                    <form class="form-horizontal" method="post" action="<?php
                    $lv = $_SESSION['logged_in_admin']['level'];
                    switch ($lv) {
                        case '0':
                            echo base_url('backend/main/gallery/uploads');
                            break;
                        case '1':
                            echo base_url('backend/user/gallery/uploads');
                            break;
                        case '2':
                            echo base_url('backend/superuser/gallery/uploads');
                            break;
                        default:
                            echo 'User Incorrect Contact Administartor!!';
                            break;
                    } // end switch
                     ?>" enctype="multipart/form-data" >
                        <div class="col-lg-9 select mb-3">

                            <select name="id_gallery" class="custom-select form-control">
                                <?php

                                foreach ($data as $key => $value)
                                {
                                    if ($id = $this->uri->segment(5) == $value->id)
                                    {
                                        // code...

                                    ?>
                                    <option value="<?php echo $value->id; ?>" <?php echo "selected";?> ><?php echo $value->gallery_name?></option>
                                    <?php
                                }else {
                                    ?>

                                    <option value="<?php echo $value->id; ?>"  ><?php echo $value->gallery_name?></option>
                                    <?php

                                }
                                    ?>
                                <?php
                                }
                                ?>
                            </select>

                        </div>

                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label d-flex justify-content-lg-end">Uploads</label>
                            <div class="col-lg-6">
                                <input type="file" class="form-control"  name="image">
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label d-flex justify-content-lg-end">Image Title</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control"  name="image_title">
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label d-flex justify-content-lg-end">Image Description</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control"  name="image_description">
                            </div>
                        </div>





                    <div class="em-separator separator-dashed"></div>
                    <div class="text-right">
                        <button class="btn btn-gradient-01" type="submit">Uploads</button>

                    </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <!-- End Row -->
</div>
