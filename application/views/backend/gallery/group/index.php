

<div class="container-fluid">
    <!-- Begin Page Header-->
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">Gallery</h2>
                <div>
                    <ul class="breadcrumb">

                        <li class="breadcrumb-item active">Gallery</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 ">
        <div class="widget has-shadow">

            <div class="widget-body">

                <a href="<?php
                $lv = $_SESSION['logged_in_admin']['level'];
                switch ($lv) {
                    case '0':
                        echo base_url('backend/main/gallery/uploads/').$this->uri->segment(5);
                        break;
                    case '1':
                        echo base_url('backend/user/gallery/uploads/').$this->uri->segment(5);
                        break;
                    case '2':
                        echo base_url('backend/superuser/gallery/uploads/').$this->uri->segment(5);
                        break;
                    default:
                        echo 'User Incorrect Contact Administartor!!';
                        break;
                } // end switch
                 ?>" class="btn btn-primary mr-1 mb-2">Upload</a>

            </div>
        </div>
    </div>

    <!-- End Page Header -->
    <div class="row flex-row">
        <div class="row">
            <?php


            foreach ($data as $key => $value) {

             ?>

 <div class="col-md-4">
     <div class="widget has-shadow col-md-12">
   <div class="thumbnail">
       <br>
     <a href="/mini/uploads/gallery/<?php echo $value->image_name?>">
       <img src="/mini/uploads/gallery/<?php echo $value->image_name?>" alt="Lights" style="width:100%">
       <div class="caption">
           <div class="col-md-12">

               <br>
         <p><?php echo $value->image_title; ?></p>
         <p>Description</p>
         </div>
       </div>
     </a>

     <a href="<?php
     $lv = $_SESSION['logged_in_admin']['level'];
     switch ($lv) {
         case '0':
             echo base_url('backend/main/gallery/uploads/setthumb').'/'.$value->id.'/'.$this->uri->segment(5);
             break;
         case '1':
             echo base_url('backend/user/gallery/uploads/setthumb').'/'.$value->id.'/'.$this->uri->segment(5);
             break;
         case '2':
             echo base_url('backend/superuser/gallery/uploads/setthumb').'/'.$value->id.'/'.$this->uri->segment(5);
             break;
         default:
             echo 'User Incorrect Contact Administartor!!';
             break;
     } // end switch
        ?>"> ตั้งเป็นปก </a>
        <br>



   </div>

</div>
 </div>
        <?php

        }

        ?>


</div>

    </div>

    <!-- End Row -->
</div>
