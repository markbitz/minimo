<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <table width="680" border="1">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Firstname</th>
                    <th>Lastname</th>
                    <th>Tel</th>
                    <th>Email</th>
                    <th>Workshop</th>
                </tr>
            </thead>
            <tbody>
                <?php
    				$count=0;
    				foreach ($data as $row)
    				{
          		?>
    			<tr>
      				<td><?php echo $count; ?></td>
      				<td><?php echo $row->firstname; ?></td>
      				<td><?php echo $row->lastname; ?></td>
      				<td><?php echo $row->tel; ?></td>
      				<td><?php echo $row->email; ?></td>
      				<td><?php echo $row->workshop; ?></td>
                </tr>
    			<?php
                        $count++;
                    } // end foreach
    			?>
            </tbody>
        </table>

    </body>
</html>
