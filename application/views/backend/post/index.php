<?php
/**
 * This is the summary for a DocBlock.
 * PHP version 5
 * This is the description for a DocBlock. This text may contain
 * multiple lines and even some _markdown_.
 *
 * * Markdown style lists function too
 * * Just try this out once
 *
 * The section after the description contains the tags; which provide
 * structured meta-data concerning the given element.
 *
 * Page-Level DocBlock
 *
 * @category MyCategory
 * @package  MyPackage
 * @author   Mike van Riel <me@mikevanriel.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://example.com/my/bar Documentation of Foo.
 *
 * @return integer Indicates the number of items.
 * @since  1.0
 *
 * Page-Level DocBlock
 */
if ($this->session->flashdata('msg-success')) {
    $this->msg->success();
} elseif ($this->session->flashdata('msg-warning')) {
        $this->msg->warning();
}
?>
<div class="row">
    <div class="page-header">
        <div class="d-flex align-items-center">
            <h2 class="page-header-title">Post</h2>
            <div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="db-all.html">
                            <i class="ti ti-home">
                            </i>
                        </a>
                    </li>
                    <li class="breadcrumb-item active">Post</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="widget has-shadow">
    <div class="widget-header bordered no-actions d-flex align-items-center">
        <h4>Post</h4>
    </div>
    <div class="widget-body">
        <div class="table-responsive">

            <table id="sorting-table" class="table mb-0">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Title</th>
                        <th>Excerpt</th>
                        <th>Categories</th>
                        <th>Auther</th>
                        <th>Status</th>
                        <th>Createdate</th>
                        <th>Updatedate</th>
                        <th>Edi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $count=1;
                    foreach ($post as $i => $row) {
                        ?>
                    <tr>
                        <td><?php echo $count;?></td>
                        <td><?php echo $row->title;?></td>
                        <td><?php echo $row->excerpt;?></td>
                        <td><?php echo $row->categories;?></td>
                        <td><?php echo $row->auther;?></td>
                        <td><?php
                        if ($row->status != 0) {
                            echo "close";
                        } else {
                            echo "open";
                        }
                        ?>
                        </td>
                        <td><?php echo $row->createdate; ?></td>
                        <td><?php echo $row->updatedate; ?></td>
                        <td class="td-actions">
                            <a href="
                            <?php
                                echo base_url('backend/post/edit/').$row->id;
                            ?>">
                                <i class="la la-edit edit">
                                </i>
                            </a>
                            <a href="
                            <?php
                                echo base_url('backend/post/delete/').$row->id;
                            ?>">
                                <i class="la la-close delete">
                                </i>
                            </a>
                        </td>
                        </tr>
                        <?php
                            $count++;
                    } // end foreach
                    ?>
                </tbody>
        </table>
        <div class="text-right">
            <a href="
            <?php
            echo base_url('backend/post/add');
            ?> " class="btn btn-secondary mr-1 mb-2" role="button">
                บันทึก
            </a>
        </div>
        </div>
            </div>
