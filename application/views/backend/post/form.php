<?php
/**
 * This is the summary for a DocBlock.
 * PHP version 5
 * This is the description for a DocBlock. This text may contain
 * multiple lines and even some _markdown_.
 *
 * * Markdown style lists function too
 * * Just try this out once
 *
 * The section after the description contains the tags; which provide
 * structured meta-data concerning the given element.
 *
 * Page-Level DocBlock
 *
 * @category MyCategory
 * @package  MyPackage
 * @author   Mike van Riel <me@mikevanriel.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://example.com/my/bar Documentation of Foo.
 *
 * @return integer Indicates the number of items.
 * @since  1.0
 *
 * Page-Level DocBlock
 */
if ($post != null) {
    foreach ($post as $key => $value) {
        $id = $value->id;
        $title = $value->title;
        $excerpt = $value->excerpt;
        $categories_post = $value->categories;
        $auther = $value->auther;
        $cover_thumbnail = $value->cover_thumbnail;
        $cover = $value->cover;
        $content = $value->content;
        $status = $value->status;
        $slug = $value->slug;
    }
} else {
        $id = null;
        $title = null;
        $excerpt = null;
        $auther = $_SESSION['logged_in_admin']['firstname'];
        $cover_thumbnail = null;
        $cover = null;
        $content = null;
        $status = null;
        $slug = null;
}

?>
<div class="container-fluid">
    <!-- Begin Page Header-->
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">Post</h2>
                <div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="db-default.html">
                                <i class="ti ti-home">
                                </i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item active">Post</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- End Page Header -->
    <div class="row flex-row">

        <div class="col-xl-9">
            <div class="widget has-shadow">
                <div class="widget-header bordered no-actions
                d-flex align-items-center">
                    <h4>Post</h4>
                </div>
                <div class="widget-body">
                    <?php
                    echo form_open('backend/post/save');
                    $hidden = array('id' => $id);
                    echo form_hidden($hidden);
                    $auther_hidden = array('auther' => $auther, );
                    echo form_hidden($auther_hidden);
                    ?>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <?php
                            $attributes = array(
                                'class' => 'col-lg-2 form-control-label
                                            d-flex justify-content-lg-end'
                                );
                            echo form_label('Title', 'title', $attributes);?>
                            <div class="col-lg-6">
                                <?php
                                    $title_option = array(
                                        'type'  => 'text',
                                        'name'  => 'title',
                                        'id'    => 'title',
                                        'class' => 'form-control',
                                        'value'=> set_value('title', $title)
                                    );
                                    echo form_input($title_option);
                                    ?>
                            </div>
                            <span class="error">*
                                <?php echo form_error('title');?>
                            </span>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <?php
                            $attributes = array(
                                'class' => 'col-lg-2 form-control-label
                                            d-flex justify-content-lg-end'
                                );
                            echo form_label('Excerpt', 'excerpt', $attributes);?>
                            <div class="col-lg-6">
                                <?php
                                    $excerpt_option = array(
                                        'type'  => 'text',
                                        'name'  => 'excerpt',
                                        'id'    => 'excerpt',
                                        'class' => 'form-control',
                                        'value'=> set_value('excerpt', $excerpt)
                                    );
                                    echo form_input($excerpt_option);
                                    ?>
                            </div>
                            <span class="error">*
                                <?php echo form_error('excerpt');?>
                            </span>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <?php
                            $attributes = array(
                                'class' => 'col-lg-2 form-control-label
                                            d-flex justify-content-lg-end'
                                );
                            echo form_label(
                                'Categories',
                                'categories',
                                $attributes
                            );
                            ?>
                            <div class="col-lg-6 select mb-3">
                                <?php
                                foreach ($categories as $key => $value) {
                                    $categories_option[$key] = array(
                                        $value->name => $value->name
                                    );
                                }
                                $categories_class = array(
                                                    'class' =>
                                                    'custom-select form-control',
                                                    );
                                if ($post == null) {
                                    echo form_dropdown(
                                        'categories',
                                        $categories_option,
                                        '',
                                        $categories_class
                                    );
                                } else {
                                    echo form_dropdown(
                                        'categories',
                                        $categories_option,
                                        $categories_post,
                                        $categories_class
                                    );
                                }
                                ?>
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <?php
                            $attributes = array(
                                'class' => 'col-lg-2 form-control-label
                                            d-flex justify-content-lg-end'
                                );
                            echo form_label(
                                'Cover thumbnail',
                                'cover_thumbnail',
                                $attributes
                            );
                            ?>
                            <div class="col-lg-6">
                                <?php
                                    $cover_thumbnail_option = array(
                                        'type'  => 'text',
                                        'name'  => 'cover_thumbnail',
                                        'id'    => 'cover_thumbnail',
                                        'class' => 'form-control',
                                        'value'=> set_value(
                                            'cover_thumbnail',
                                            $cover_thumbnail
                                        )
                                    );
                                    echo form_input($cover_thumbnail_option);
                                    ?>
                            </div>
                            <span class="error">*
                                <?php echo form_error('cover_thumbnail');?>
                            </span>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <?php
                            $attributes = array(
                                'class' => 'col-lg-2 form-control-label
                                            d-flex justify-content-lg-end'
                                );
                            echo form_label('Cover', 'cover', $attributes);?>
                            <div class="col-lg-6">
                                <?php
                                    $cover_option = array(
                                        'type'  => 'text',
                                        'name'  => 'cover',
                                        'id'    => 'cover',
                                        'class' => 'form-control',
                                        'value'=> set_value('cover', $cover)
                                    );
                                    echo form_input($cover_option);
                                    ?>
                            </div>
                            <span class="error">*
                                <?php echo form_error('cover');?>
                            </span>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <?php
                            $attributes = array(
                                'class' => 'col-lg-2 form-control-label
                                            d-flex justify-content-lg-end'
                                );
                            echo form_label('Slug', 'slug', $attributes);?>
                            <div class="col-lg-6">
                                <?php
                                    $slug_option = array(
                                        'type'  => 'text',
                                        'name'  => 'slug',
                                        'id'    => 'slug',
                                        'class' => 'form-control',
                                        'value'=> set_value('slug', $slug)
                                    );
                                    echo form_input($slug_option);
                                    ?>
                            </div>
                            <span class="error">*
                                <?php echo form_error('slug');?>
                            </span>
                        </div>
                        <div class="form-group row mb-5">
                            <?php
                            $attributes = array(
                                'class' => 'col-lg-2 form-control-label
                                            d-flex justify-content-lg-end'
                                );
                            echo form_label('ค่าสถานะ', 'status', $attributes);?>
                            <div class="col-lg-6 select mb-3">
                                <?php
                                $options_class = array(
                                                    'class' =>
                                                    'custom-select form-control',
                                                    );
                                $options = array(
                                    '0'   => 'Open',
                                    '1'   => 'Close'
                                );
                                if ($post == null) {
                                    echo form_dropdown(
                                        'status',
                                        $options,
                                        '',
                                        $options_class
                                    );
                                } else {
                                    echo form_dropdown(
                                        'status',
                                        $options,
                                        $status,
                                        $options_class
                                    );
                                }
                                ?>

                            </div>
                        </div>
                        <div class="form-group row d-flex  mb-5">
                            <?php
                            $attributes = array(
                                'class' => 'col-lg-2 form-control-label
                                            d-flex justify-content-lg-end'
                                );
                            echo form_label('Content', 'content', $attributes);?>
                            <div class="col-lg-9">
                                <?php
                                $content_option = array(
                                    'name'        => 'content',
                                    'id'          => 'content',
                                    'value'       => set_value(
                                        'content',
                                        $content
                                    ),
                                    'rows'        => '20',
                                    'class'       => 'form-control'
                                );

                                echo form_textarea($content_option);
                                ?>
                            </div>
                            <span class="error">*
                                <?php echo form_error('content');?>
                            </span>
                        </div>

                        <div class="em-separator separator-dashed"></div>
                            <div class="text-right">
                                <?php
                                if ($post == null) {
                                    $submit = array(
                                            'name'          => 'button',
                                            'id'            => 'button',
                                            'value'         => 'true',
                                            'type'          => 'submit',
                                            'class'         => 'btn btn-gradient-01',
                                            'content'       => 'บันทึก'
                                            );
                                            echo form_button($submit);
                                    $reset = array(
                                            'name'          => 'button',
                                            'id'            => 'button',
                                            'value'         => 'true',
                                            'type'          => 'reset',
                                            'class'         => 'btn btn-gradient-01',
                                            'content'       => 'Reset'
                                            );
                                            echo form_button($reset);
                                } else {
                                    $submit = array(
                                            'name'          => 'button',
                                            'id'            => 'button',
                                            'value'         => 'true',
                                            'type'          => 'submit',
                                            'class'         => 'btn btn-gradient-01',
                                            'content'       => 'แก้ไข'
                                            );
                                            echo form_button($submit);
                                }

                                ?>

                            </div>
                            <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    <!-- End Row -->
</div>
