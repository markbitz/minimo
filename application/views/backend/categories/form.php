<?php
    if ($categories != NULL) {
        foreach ($categories as $key => $value) {
            $id = $value->id;
            $name = $value->name;
            $status = $value->status;
        }
    }else {
        $id = NULL;
        $name = NULL;
        $status = NULL;
    }
 ?>
<div class="container-fluid">
    <!-- Begin Page Header-->
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">Profile</h2>
                <div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="db-default.html"><i class="ti ti-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item active">Categories</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- End Page Header -->
    <div class="row flex-row">

        <div class="col-xl-9">
            <div class="widget has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h4>Update Categories</h4>
                </div>
                <div class="widget-body">
                    <?php
                    echo form_open('backend/categories/save');
                    $hidden = array('id' => $id,);
                    echo form_hidden($hidden);
                    ?>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <?php
                            $attributes = array(
                                'class' => 'col-lg-2 form-control-label d-flex justify-content-lg-end',
                                'for' => 'name'
                                );
                            echo form_label('Create Categories', 'name',$attributes);?>
                            <div class="col-lg-6">
                                <?php
                                    $name_option = array(
                                        'type'  => 'text',
                                        'name'  => 'name',
                                        'id'    => 'name',
                                        'class' => 'form-control',
                                        'value'=> set_value('name',$name)
                                    );
                                    echo form_input($name_option);
                                 ?>
                            </div>
                            <span class="error">* <?php echo form_error('name');?></span>
                        </div>
                        <div class="form-group row mb-5">
                            <label class="col-lg-2 form-control-label">ค่าสถานะ</label>
                            <div class="col-lg-6 select mb-3">
                                <?php
                                $options = array(
                                    '0'   => 'Show Menu',
                                    '1'   => 'Notshow Menu'
                                );
                                if ($categories == NULL) {
                                    echo form_dropdown('status', $options);
                                } else {
                                    echo form_dropdown('status', $options,$status);
                                }

                                 ?>

                            </div>
                        </div>
                        <div class="em-separator separator-dashed"></div>
                            <div class="text-right">
                                <?php
                                if ($categories == NULL) {
                                    $submit = array(
                                        'name'          => 'button',
                                        'id'            => 'button',
                                        'value'         => 'true',
                                        'type'          => 'submit',
                                        'class'          => 'btn btn-gradient-01',
                                        'content'       => 'บันทึก'
                                        );
                                        echo form_button($submit);
                                        $reset = array(
                                            'name'          => 'button',
                                            'id'            => 'button',
                                            'value'         => 'true',
                                            'type'          => 'reset',
                                            'class'          => 'btn btn-gradient-01',
                                            'content'       => 'Reset'
                                            );
                                            echo form_button($reset);
                                } else {
                                    $submit = array(
                                        'name'          => 'button',
                                        'id'            => 'button',
                                        'value'         => 'true',
                                        'type'          => 'submit',
                                        'class'          => 'btn btn-gradient-01',
                                        'content'       => 'แก้ไข'
                                        );
                                        echo form_button($submit);
                                }

                                ?>

                            </div>
                            <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    <!-- End Row -->
</div>
