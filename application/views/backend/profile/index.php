<div class="container-fluid">
    <!-- Begin Page Header-->
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">Profile</h2>
                <div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="db-default.html"><i class="ti ti-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item active">Profile</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php



    foreach ($data as $i => $row)
    {
    ?>
    <!-- End Page Header -->
    <div class="row flex-row">
        <div class="col-xl-3">
            <!-- Begin Widget -->
            <div class="widget has-shadow">
                <div class="widget-body">
                    <div class="mt-5">
                        <img src="<?php echo base_url('uploads/avatar/'). $row->avatar ; ?>" alt="..." style="width: 120px;" class="avatar rounded-circle d-block mx-auto">
                    </div>
                    <h3 class="text-center mt-3 mb-1"><?php echo $row->firstname;  ?></h3>
                    <p class="text-center"><?php echo $row->email; ?></p>
                    <a class="nav-link" href="<?php
                    $lv = $_SESSION['logged_in_admin']['level'];
                    switch ($lv) {
                        case '0':
                            echo base_url('backend/main/uploads_avatar_form');
                            break;
                        case '1':
                            echo base_url('backend/user/uploads_avatar_form');
                            break;
                        case '2':
                            echo base_url('backend/superuser/uploads_avatar_form');
                            break;
                        default:
                            echo 'User Incorrect Contact Administartor!!';
                            break;
                    } // end switch

                    ?>"><i class="la la-2x align-middle pr-2"></i> Upload Avatar</a>
                    <div class="em-separator separator-dashed"></div>

                </div>
            </div>
            <!-- End Widget -->
        </div>
        <div class="col-xl-9">
            <div class="widget has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h4>Update Profile</h4>
                </div>
                <div class="widget-body">

                    <form class="form-horizontal" method="post" action="
                    <?php
                    $lv = $_SESSION['logged_in_admin']['level'];
                    switch ($lv) {
                        case '0':
                            echo base_url('backend/main/editprofile');
                            break;
                        case '1':
                            echo base_url('backend/user/edit');
                            break;
                        case '2':
                            echo base_url('backend/superuser/edit');
                            break;
                        default:
                            echo 'User Incorrect Contact Administartor!!';
                            break;
                    } // end switch

                    ?>">
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label d-flex justify-content-lg-end">First Name</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" placeholder="First Name" name="firstname"
                                value="<?php echo $row->firstname; ?>">
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label d-flex justify-content-lg-end">Last Name</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" placeholder="Last Name"name="lastname"
                                value="<?php echo $row->lastname; ?>">
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label d-flex justify-content-lg-end">Email</label>
                            <div class="col-lg-6">
                                <input type="email" class="form-control" placeholder="dgreen@mail.com" name="email"
                                value="<?php echo $row->email; ?>">
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label d-flex justify-content-lg-end">Phone</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" placeholder="+00 987 654 32"name="tel"
                                value="<?php echo $row->tel; ?>">
                            </div>
                        </div>
                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label d-flex justify-content-lg-end">Password</label>
                            <div class="col-lg-6">
                                <input type="password" class="form-control"  name="password"
                                value="<?php  ?>">
                            </div>
                        </div>




                    <div class="em-separator separator-dashed"></div>
                    <div class="text-right">
                        <button class="btn btn-gradient-01" type="submit">Save Changes</button>

                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Row -->
</div>
<?php
}
?>
