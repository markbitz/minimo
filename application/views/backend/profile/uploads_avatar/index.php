<div class="container-fluid">
    <!-- Begin Page Header-->
    <div class="row">
        <div class="page-header">
            <div class="d-flex align-items-center">
                <h2 class="page-header-title">Profile</h2>
                <div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="db-default.html"><i class="ti ti-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item active">Profile</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- End Page Header -->
    <div class="row flex-row">


        <div class="col-xl-9">
            <div class="widget has-shadow">
                <div class="widget-header bordered no-actions d-flex align-items-center">
                    <h4>Update Profile</h4>
                </div>
                <div class="widget-body">


                    <form class="form-horizontal" method="post" action="
                    <?php
                    $lv = $_SESSION['logged_in_admin']['level'];
                    switch ($lv) {
                        case '0':
                            echo base_url('backend/main/uploads_avatar') ;
                            break;
                        case '1':
                            echo base_url('backend/user/uploads_avatar') ;
                            break;
                        case '2':
                            echo base_url('backend/superuser/uploads_avatar') ;
                            break;
                        default:
                            echo 'User Incorrect Contact Administartor!!';
                            break;
                    } // end switch
                        ?>" enctype="multipart/form-data" >

                        <div class="form-group row d-flex align-items-center mb-5">
                            <label class="col-lg-2 form-control-label d-flex justify-content-lg-end">Uploads</label>
                            <div class="col-lg-6">
                                <input type="file" class="form-control"  name="image">
                            </div>
                        </div>







                    <div class="em-separator separator-dashed"></div>
                    <div class="text-right">
                        <button class="btn btn-gradient-01" type="submit">Uploads</button>

                    </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <!-- End Row -->
</div>
