<?php
/**
 * This is the summary for a DocBlock.
 * PHP version 5
 * This is the description for a DocBlock. This text may contain
 * multiple lines and even some _markdown_.
 *
 * * Markdown style lists function too
 * * Just try this out once
 *
 * The section after the description contains the tags; which provide
 * structured meta-data concerning the given element.
 *
 * Page-Level DocBlock
 *
 * @category MyCategory
 * @package  MyPackage
 * @author   Mike van Riel <me@mikevanriel.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://example.com/my/bar Documentation of Foo.
 *
 * @return integer Indicates the number of items.
 * @since  1.0
 *
 * Page-Level DocBlock
 */
foreach ($post as $key => $value) {
        $categories = $value->categories;
        $title = $value->title;
        $content = $value->content;
        $cover_thumbnail = $value->cover_thumbnail;
        $cover = $value->cover;
}
?>
<div class="container">
    <div class="row">
        <div class="text-center col-xs-12 col-md-offset-1   col-md-10">
            <img src="<?php echo $cover; ?> "
            class="img-responsive" alt="Smiley face"  style="width:98%" >
        <br>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="caption">
            <div class="text-left col-xs-12 col-md-offset-2 col-md-8">
                <h5><?php echo $categories; ?></h5>
                <h3><?php echo $title; ?></h3>
                <br>
                <p><?php echo $content; ?></p>
                <br><br>
                <h5>Leave a comment</h5>
                <br><br><br><br>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-offset-2">
        <?php
        foreach ($results as $key => $value) {
            ?>
            <div class="  col-md-5">
                <div class="caption">
                    <img src="<?php echo $value->cover_thumbnail; ?> "
                    class="img-responsive" width="98%" alt="Image">
                    <h5 ><?php echo $value->categories; ?></h5>
                    <h3 ><?php echo $value->title; ?></h3>
                    <p > <?php echo $value->content; ?> </p>
                    <br><br><br><br>
                </div>
            </div>
            <?php
        }
        ?>
        </div>
    </div>
    <div class="row">
        <div class=" col-xs-offset-5 col-md-offset-5 ">
            <?php echo $links; ?>
        </div>
    </div>
</div>
<div class="jumbotron">
    <div class="container text-center">
        <h3 class="col-md-offset-1">Sign up for our newsletter!</h3>
        <br>
        <form>
            <div class="col-md-offset-5 col-md-3 ">
                <div class="input-container">
                    <div class="input-group">
                        <div class="group">
                            <input type="text" required>
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label>Email</label>
                        </div>
                        <span class="input-group-btn">
                            <button type="submit" />
                            <i class="material-icons">send</i>
                        </span>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
