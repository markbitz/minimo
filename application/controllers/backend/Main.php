<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('Main_model','main');
		$this->load->model('Gallery_model','gallery');
		$this->load->model('Categories_model','categories');
		$this->load->model('Seo_model','seo');

        if($_SESSION['logged_in_admin']['level']!=0){

            redirect("login/logout", "refresh");

        }else {
        	is_logged_in_admin();
        }
    }
	public function index()
	{
		$this->data['data']=$this->main->findAll();
        $this->data['css'] = array(
            'assets/css/bootstrap.min.css' ,
            'assets/css/elisyam-1.5.min.css',
			'assets/plugin/css/datatables/datatables.min.css'
        );
		$this->data['js'] = array(
            'assets/js/jquery.min.js' ,
            'assets/js/core.min.js',
			"assets/plugin/js/datatables/datatables.min.js",
	        "assets/plugin/js/datatables/dataTables.buttons.min.js",
	        "assets/plugin/js/datatables/jszip.min.js",
	        "assets/plugin/js/datatables/buttons.html5.min.js",
	       	"assets/plugin/js/datatables/pdfmake.min.js",
	        "assets/plugin/js/datatables/vfs_fonts.js",
	        "assets/plugin/js/datatables/buttons.print.min.js",
	        "assets/plugin/js/nicescroll/nicescroll.min.js",
	        "assets/plugin/js/app/app.min.js",
			"assets/plugin/js/components/tables/tables.js"
        );
        $this->layout_admin("backend/main/index",$this->data);
	}
	public function insertform()
	{
		$this->data['data']=NULL;
		//$this->data['data']=$this->main->findAll();
        $this->data['css'] = array(
            'assets/css/bootstrap.min.css' ,
            'assets/css/elisyam-1.5.min.css'
        );
		$this->data['js'] = array(
            'assets/js/jquery.min.js' ,
            'assets/js/core.min.js',
            'assets/plugin/js/nicescroll/nicescroll.min.js' ,
            'assets/plugin/js/app/app.min.js',
        );
        $this->layout_admin("backend/insert/index",$this->data);
	}
	public function insert()
	{
		$options=['cost' => 12];
        $crypt=$this->security->xss_clean($this->input->post('password'));
        $password = password_hash($crypt,PASSWORD_BCRYPT,$options);
		$data = array('email' => $this->input->post('email'),
			'password' => $password,
			'firstname' => $this->input->post('firstname'),
			'lastname' => $this->input->post('lastname'),
			'level' => $this->input->post('level'),
			'tel' => $this->input->post('tel'),
			'createdate'=>$this->dateTimeNow
		);
		$this->main->save($data);
		redirect('backend');

	}
	public function editform()
	{
		$id=$this->uri->segment(3);
		$this->data['data']=$this->main->findById($id);

		$this->data['css'] = array(
            'assets/css/bootstrap.min.css' ,
            'assets/css/elisyam-1.5.min.css'
        );
		$this->data['js'] = array(
            'assets/js/jquery.min.js' ,
            'assets/js/core.min.js',
            'assets/plugin/js/nicescroll/nicescroll.min.js' ,
            'assets/plugin/js/app/app.min.js',

        );
        $this->layout_admin("backend/insert/index",$this->data);

	}
	public function edit()
	{
		if($this->input->post('password') != NULL){
		$id = $this->input->post('id');
		$options=['cost' => 12];
        $crypt=$this->security->xss_clean($this->input->post('password'));
        $password = password_hash($crypt,PASSWORD_BCRYPT,$options);
		$data = array(
			'email' => $this->input->post('email'),
			'password' => $password,
			'firstname' => $this->input->post('firstname'),
			'lastname' => $this->input->post('lastname'),
			'level' => $this->input->post('level'),
			'tel' => $this->input->post('tel'),
			'updatedate'=>$this->dateTimeNow
		);
		$this->main->update($id,$data);
		redirect('backend');
	}else {
		$id = $this->input->post('id');

		$data = array(
			'email' => $this->input->post('email'),
			'firstname' => $this->input->post('firstname'),
			'lastname' => $this->input->post('lastname'),
			'level' => $this->input->post('level'),
			'tel' => $this->input->post('tel'),
			'updatedate'=>$this->dateTimeNow
		);
		$this->main->update($id,$data);
		redirect('backend');
	}
	}
	public function editprofile()
	{
		$id = $_SESSION['logged_in_admin']['id'];
		if($this->input->post('password') != NULL){

			$options=['cost' => 12];
        	$crypt=$this->security->xss_clean($this->input->post('password'));

        	$password = password_hash($crypt,PASSWORD_BCRYPT,$options);
			$data = array(
				'email' => $this->input->post('email'),
				'password' => $password,
				'firstname' => $this->input->post('firstname'),
				'lastname' => $this->input->post('lastname'),

				'tel' => $this->input->post('tel'),
				'updatedate'=>$this->dateTimeNow
			);
			$this->main->update($id,$data);
			redirect('backend');
		}else {
			$data = array(
				'email' => $this->input->post('email'),
				'firstname' => $this->input->post('firstname'),
				'lastname' => $this->input->post('lastname'),

				'tel' => $this->input->post('tel'),
				'updatedate'=>$this->dateTimeNow
			);
			$this->main->update($id,$data);
			redirect('backend/main');
		}
	}
	public function delete()
	{
		$id=$this->uri->segment(3);
		//$id = $this->input->post('id');
		$this->main->delete($id);
		redirect('backend');
	}
	public function uploads_avatar_form()
	{
		$this->data['css'] = array(
            'assets/css/bootstrap.min.css' ,
            'assets/css/elisyam-1.5.min.css',
			'assets/plugin/css/datatables/datatables.min.css',
            'assets/plugin/css/owl-carousel/owl.carousel.min.css',
            'assets/plugin/css/owl-carousel/owl.theme.min.css'
        );
		$this->data['js'] = array(
            'assets/js/jquery.min.js' ,
            'assets/js/core.min.js',
	        'assets/plugin/js/nicescroll/nicescroll.min.js',
            'assets/plugin/js/owl-carousel/owl.carousel.min.js',
	        'assets/plugin/js/app/app.min.js',
			'assets/plugin/js/app/contact/contact.min.js'
        );
        $this->layout_user("backend/profile/uploads_avatar/index",$this->data);
	}
	public function uploads_avatar()
	{

		$dateTimeNow = date("YmdHis");
		$id_user= $_SESSION['logged_in_admin']['id'];
		//$this->data['data']=$this->gallery->findAll();
		$config['upload_path']          = './uploads/avatar/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['overwrite']            = TRUE;
		$config['max_size']             = 5120;
		$config['min_width']            = 1024;
		$config['min_height']           = 768;
		$this->load->library('upload', $config);
		$this->upload->do_upload('image');
		$upload_data = $this->upload->data();
		$config['image_library'] = 'gd2';
		$config['source_image'] = $upload_data['full_path'];
		//$config['create_thumb'] = TRUE;
		//$config['new_image'] = './uploads/gallery/tumb/user'.$id_user.'gallery'.$this->input->post('id_gallery').'.png';
		$config['maintain_ratio'] = TRUE;
		$config['width']         = 512;
		$config['height']       = 512;
		$this->load->library('image_lib', $config);

		$this->image_lib->resize();
		$image_name =rename ( $upload_data['full_path'] , $upload_data['file_path'].$dateTimeNow.'user'.$id_user.'avatar.png' );


		$data = array(
			'avatar' => $dateTimeNow.'user'.$id_user.'avatar.png',
			'id' => $id_user,
			'updatedate'=>$this->dateTimeNow
		);
		/*$datatump = array(
			'image_name' => $dateTimeNow.'gallery'.$this->input->post('id_gallery').'_thumb.png',
			'updatedate'=>$this->dateTimeNow
		);
		$this->gallery->update_thumb($id,$datatump);*/
		$this->main->update($id_user,$data);
		redirect('backend/main');
	}
	public function profile()
	{
        $id= $_SESSION['logged_in_admin']['id'];
		$this->data['data']=$this->main->findById($id);
        $this->data['css'] = array(
            'assets/css/bootstrap.min.css' ,
            'assets/css/elisyam-1.5.min.css',
			'assets/plugin/css/datatables/datatables.min.css',
            'assets/plugin/css/owl-carousel/owl.carousel.min.css',
            'assets/plugin/css/owl-carousel/owl.theme.min.css'
        );
		$this->data['js'] = array(
            'assets/js/jquery.min.js' ,
            'assets/js/core.min.js',
	        'assets/plugin/js/nicescroll/nicescroll.min.js',
            'assets/plugin/js/owl-carousel/owl.carousel.min.js',
	        'assets/plugin/js/app/app.min.js',
			'assets/plugin/js/app/contact/contact.min.js'

        );
        $this->layout_admin("backend/profile/index",$this->data);
	}
	public function gallery()
	{
		$this->data['data'] = $this->gallery->gallerythumbCount();
		$this->data['css'] = array(
            'assets/css/bootstrap.min.css' ,
            'assets/css/elisyam-1.5.min.css',
			'assets/plugin/css/datatables/datatables.min.css',
            'assets/plugin/css/owl-carousel/owl.carousel.min.css',
            'assets/plugin/css/owl-carousel/owl.theme.min.css'
        );
		$this->data['js'] = array(
            'assets/js/jquery.min.js',
            'assets/js/core.min.js',
	        'assets/plugin/js/nicescroll/nicescroll.min.js',
            'assets/plugin/js/owl-carousel/owl.carousel.min.js',
	        'assets/plugin/js/app/app.min.js',
			'assets/plugin/js/app/contact/contact.min.js'
        );
        $this->layout_admin("backend/gallery/index",$this->data);
	}
	public function gallery_group()
	{
		$id_gallery=$this->uri->segment(5);

		//$id_user = $_SESSION['logged_in_admin']['id'];
		$this->data['data'] = $this->gallery->gallery_group_findById($id_gallery);

		$this->data['css'] = array(
            'assets/css/bootstrap.min.css' ,
            'assets/css/elisyam-1.5.min.css',
			'assets/plugin/css/datatables/datatables.min.css',
            'assets/plugin/css/owl-carousel/owl.carousel.min.css',
			'assets/css/style.css',
            'assets/plugin/css/owl-carousel/owl.theme.min.css'
        );
		$this->data['js'] = array(
            'assets/js/jquery.min.js' ,
            'assets/js/core.min.js',
			'assets/js/clipboard.min.js',
			'assets/js/my.js',
	        'assets/plugin/js/nicescroll/nicescroll.min.js',
            'assets/plugin/js/owl-carousel/owl.carousel.min.js',
	        'assets/plugin/js/app/app.min.js',
			'assets/plugin/js/app/contact/contact.min.js'

        );

        $this->layout_admin("backend/gallery/group/index",$this->data);

	}
	public function setthumb()
	{

		$id_image = $this->uri->segment(6);
		$id_gallery = $this->uri->segment(7);

		//$id_user = $_SESSION['logged_in_admin']['id'];

		$this->data['data']=$this->gallery->gallery_thumb_findById($id_gallery,$id_image);

		foreach ($this->data['data'] as $key => $value) {
			$config['image_library'] = 'gd2';
			$config['source_image'] = './uploads/gallery/'.$value->image_name;
			$config['maintain_ratio'] = TRUE;
			$config['create_thumb'] = TRUE;
			$config['overwrite'] = TRUE;
			$config['new_image'] = './uploads/gallery/thumb/gallery'.$id_gallery.'.png';
			$config['width']         = 1024;
			$config['height']       = 683;
			$this->load->library('image_lib', $config);
			$this->image_lib->resize();
			$filename = 'gallery'.$id_gallery.'_thumb.png';
			$data = array(
				'tump_image' => $filename,
				//'status' => 1,

				'updatedate'=>$this->dateTimeNow
			);
			$this->gallery->update($id_gallery,$data);
			redirect('backend/main/gallery');
		}
	}
	public function create_gallery_form()
	{
		$this->data['css'] = array(
            'assets/css/bootstrap.min.css' ,
            'assets/css/elisyam-1.5.min.css',
			'assets/plugin/css/datatables/datatables.min.css',
            'assets/plugin/css/owl-carousel/owl.carousel.min.css',
            'assets/plugin/css/owl-carousel/owl.theme.min.css'
        );
		$this->data['js'] = array(
            'assets/js/jquery.min.js' ,
            'assets/js/core.min.js',
	        'assets/plugin/js/nicescroll/nicescroll.min.js',
            'assets/plugin/js/owl-carousel/owl.carousel.min.js',
	        'assets/plugin/js/app/app.min.js',
			'assets/plugin/js/app/contact/contact.min.js'

        );

        $this->layout_admin("backend/gallery/create_gallery/index",$this->data);

	}

	public function create_gallery()
	{

		$id = $_SESSION['logged_in_admin']['id'];

		$data = array(
			'gallery_name' => $this->input->post('namegallery'),
			'id_user' => $id,
			'createdate'=>$this->dateTimeNow
		);
		$this->categories->save($data);
		redirect('backend/gallery');

	}
	public function uploads()
	{
		$dateTimeNow = date("YmdHis");
		$id_user= $_SESSION['logged_in_admin']['id'];
		//$this->data['data']=$this->gallery->findAll();
		$config['upload_path']          = './uploads/gallery/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['overwrite']            = TRUE;
		$config['max_size']             = 5120;
		$config['min_width']            = 1024;
		$config['min_height']           = 768;
		$this->load->library('upload', $config);
		$this->upload->do_upload('image');
		$upload_data = $this->upload->data();
		$config['image_library'] = 'gd2';
		$config['source_image'] = $upload_data['full_path'];
		//$config['create_thumb'] = TRUE;
		//$config['new_image'] = './uploads/gallery/tumb/user'.$id_user.'gallery'.$this->input->post('id_gallery').'.png';
		$config['maintain_ratio'] = TRUE;
		$config['width']         = 1280;
		$config['height']       = 720;
		$this->load->library('image_lib', $config);

		$this->image_lib->resize();
		$image_name =rename ( $upload_data['full_path'] , $upload_data['file_path'].$dateTimeNow.'user'.$id_user.'gallery'.$this->input->post('id_gallery').'.png' );


		$data = array(
			'image_name' => $dateTimeNow.'user'.$id_user.'gallery'.$this->input->post('id_gallery').'.png',
			'id_user' => $id_user,
			'id_gallery' => $this->input->post('id_gallery'),
			'image_title' => $this->input->post('image_title'),
			'image_description' => $this->input->post('image_description'),
			'createdate'=>$this->dateTimeNow
		);
		/*$datatump = array(
			'image_name' => $dateTimeNow.'gallery'.$this->input->post('id_gallery').'_thumb.png',
			'updatedate'=>$this->dateTimeNow
		);
		$this->gallery->update_thumb($id,$datatump);*/
		$this->gallery->save_gallery($data);

		redirect('backend/main/gallery/group/'.$this->input->post('id_gallery'));


	}
	public function uploads_form()
	{

		$this->data['data']=$this->gallery->findAll();
		$this->data['css'] = array(
            'assets/css/bootstrap.min.css' ,
            'assets/css/elisyam-1.5.min.css',
			'assets/plugin/css/datatables/datatables.min.css',
            'assets/plugin/css/owl-carousel/owl.carousel.min.css',
            'assets/plugin/css/owl-carousel/owl.theme.min.css'
        );
		$this->data['js'] = array(
            'assets/js/jquery.min.js' ,
            'assets/js/core.min.js',
	        'assets/plugin/js/nicescroll/nicescroll.min.js',
            'assets/plugin/js/owl-carousel/owl.carousel.min.js',
	        'assets/plugin/js/app/app.min.js',
			'assets/plugin/js/app/contact/contact.min.js'

        );

        $this->layout_admin("backend/gallery/uploads/index",$this->data);

	}



}
