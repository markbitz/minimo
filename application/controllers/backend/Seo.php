<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Seo extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->load->model('Seo_model','seo');
        is_logged_in_admin();
    }

    public function form()
	{
        $id = $this->uri->segment(4);
		$this->data['css'] = array(
			'assets/css/bootstrap.min.css' ,
			'assets/css/elisyam-1.5.min.css'
		);
		$this->data['js'] = array(
			'assets/js/jquery.min.js' ,
			'assets/js/core.min.js',
			'assets/plugin/js/nicescroll/nicescroll.min.js' ,
			'assets/plugin/js/app/app.min.js',
		);
        if ($id == NULL) {
            $this->data['seo'] = $this->seo->findAll();
            $this->layout_admin("backend/seo/form",$this->data);
        } else {
            $this->data['seo']=$this->seo->findById($id);
            $this->layout_admin("backend/seo/form",$this->data);
        }
    }
    public function save()
	{
		$id = $this->input->post('id');
		$config = array(
		array(
				'field' => 'url',
				'label' => 'Url',
				'rules' => 'required|valid_url'
			),
		array(
				'field' => 'type',
				'label' => 'Type',
				'rules' => 'required'
			),
		array(
				'field' => 'title',
				'label' => 'Title',
				'rules' => 'required'
			),
		array(
				'field' => 'description',
				'label' => 'Description',
				'rules' => 'required'
			),
		array(
				'field' => 'image',
				'label' => 'Image',
				'rules' => 'required|valid_url'
			),
		array(
				'field' => 'site_name',
				'label' => 'Site_name',
				'rules' => 'required'
			)
		);
		$this->form_validation->set_rules($config);
		if($this->form_validation->run()==false) {
			$this->form();
		}else {
			foreach ($this->input->post() as $key => $value) {
				$data[$key] = $this->security->xss_clean($value);
			}
			unset($data['button']);
			if ($data['id'] == NULL) {
				if ($this->seo->save($data) == false) {
					$this->session->set_flashdata('msg-warning','บันทึกไม่สำเร็จ');
				} else {
					$this->session->set_flashdata('msg-success','บันทึกสำเร็จ');
				}
			} else {
				if ($this->seo->update($id,$data) == false) {
					$this->session->set_flashdata('msg-warning','บันทึกไม่สำเร็จ');
				}else {
					$this->session->set_flashdata('msg-success','บันทึกสำเร็จ');
				}
			}
			redirect('backend/seo/add');
		}
	}
    public function delete()
	{
		$id=$this->uri->segment(4);
		if ($this->seo->delete($id) == false) {
			$this->session->set_flashdata('msg-warning','ลบข้อมูลไม่สำเร็จ');
		} else {
			$this->session->set_flashdata('msg-success','ลบข้อมูลสำเร็จ');
		}
		redirect('backend/seo');

	}
}
