<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends MY_Controller {
	public function __construct()
    {
        parent::__construct();
		$this->load->model('Account_model','account');
        if($_SESSION['logged_in_admin']['level']!=0){
            redirect("login/logout", "refresh");
        }else {
        	is_logged_in_admin();
        }
    }
	public function index()
	{
		$this->data['data']=$this->account->findAll();
        $this->data['css'] = array(
            'assets/css/bootstrap.min.css' ,
            'assets/css/elisyam-1.5.min.css',
			'assets/plugin/css/datatables/datatables.min.css'
        );
		$this->data['js'] = array(
            'assets/js/jquery.min.js' ,
            'assets/js/core.min.js',
			"assets/plugin/js/datatables/datatables.min.js",
	        "assets/plugin/js/datatables/dataTables.buttons.min.js",
	        "assets/plugin/js/datatables/buttons.html5.min.js",
	        "assets/plugin/js/datatables/vfs_fonts.js",
	        "assets/plugin/js/nicescroll/nicescroll.min.js",
	        "assets/plugin/js/app/app.min.js",
			"assets/plugin/js/components/tables/tables.js"
        );
        $this->layout_admin("backend/account/index",$this->data);

	}
	public function form()
	{
        $id=$this->uri->segment(3);
		$this->data['css'] = array(
			'assets/css/bootstrap.min.css' ,
			'assets/css/elisyam-1.5.min.css'
		);
		$this->data['js'] = array(
			'assets/js/jquery.min.js' ,
			'assets/js/core.min.js',
			'assets/plugin/js/nicescroll/nicescroll.min.js' ,
			'assets/plugin/js/app/app.min.js'
		);
        if ($id == NULL) {
            $this->data['account']=NULL;
            $this->layout_admin("backend/account/form",$this->data);
        } else {
            $this->data['account']=$this->account->findById($id);
            $this->layout_admin("backend/account/form",$this->data);
        }
	}
	public function save()
	{
		$id = $this->input->post('id');
		if ($id == NULL) {
			$config = array(
	        array(
	                'field' => 'email',
	                'label' => 'Email',
	                'rules' => 'required|valid_email'
	        	),
			array(
		            'field' => 'password',
		            'label' => 'Password',
		            'rules' => 'required'
		        ),
			array(
			        'field' => 'passwordcon',
			        'label' => 'Passwordcon',
			        'rules' => 'required|matches[password]'
				),
			array(
				    'field' => 'firstname',
				    'label' => 'Firstname',
				    'rules' => 'required'
				),
			array(
					'field' => 'lastname',
					'label' => 'Lastname',
					'rules' => 'required'
				),
			array(
					'field' => 'tel',
					'label' => 'Tel',
					'rules' => 'required'
				)
			);
		} else {
			$config = array(
	        array(
	                'field' => 'email',
	                'label' => 'Email',
	                'rules' => 'required|valid_email'
	        	),
			array(
				    'field' => 'firstname',
				    'label' => 'Firstname',
				    'rules' => 'required'
				),
			array(
					'field' => 'lastname',
					'label' => 'Lastname',
					'rules' => 'required'
				),
			array(
					'field' => 'tel',
					'label' => 'Tel',
					'rules' => 'required'
				)
			);
		}
		$this->form_validation->set_rules($config);
		if($this->form_validation->run()==false) {
            $this->form();
        } else {
			foreach ($this->input->post() as $key => $value) {
				$data[$key] = $this->security->xss_clean($value);
			}
			$options=['cost' => 12];
	        $crypt=$data['password'];
	        $data['password'] = password_hash($crypt,PASSWORD_BCRYPT,$options);
			unset($data['button']);
			unset($data['passwordcon']);
			if ($data['id'] == NULL) {

				if ($this->account->save($data) == false) {
					$this->session->set_flashdata('msg-warning','บันทึกไม่สำเร็จ');
				} else {
					$this->session->set_flashdata('msg-success','บันทึกสำเร็จ');
				}
			} else {
				unset($data['password']);
				if ($this->account->update($id,$data) == false) {
					$this->session->set_flashdata('msg-warning','บันทึกไม่สำเร็จ');
				}else {
					$this->session->set_flashdata('msg-success','บันทึกสำเร็จ');
				}
			}
    		redirect('backend');
		}
	}
	public function form_reset_password()
	{
        $id=$this->uri->segment(3);
		$this->data['css'] = array(
			'assets/css/bootstrap.min.css' ,
			'assets/css/elisyam-1.5.min.css'
		);
		$this->data['js'] = array(
			'assets/js/jquery.min.js' ,
			'assets/js/core.min.js',
			'assets/plugin/js/nicescroll/nicescroll.min.js' ,
			'assets/plugin/js/app/app.min.js'
		);
            $this->data['account']=$this->account->findById($id);
            $this->layout_admin("backend/account/form_resetpassword",$this->data);
	}
	public function reset_password()
	{
		$id = $this->input->post('id');
		$config = array(
		array(
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'required'
			),
		array(
				'field' => 'passwordcon',
				'label' => 'Passwordcon',
				'rules' => 'required|matches[password]'
			),
		);
		$this->form_validation->set_rules($config);
		if($this->form_validation->run()==false) {
            $this->form();
        } else {
			foreach ($this->input->post() as $key => $value) {
				$data[$key] = $this->security->xss_clean($value);
			}
			$options=['cost' => 12];
	        $crypt=$data['password'];
	        $data['password'] = password_hash($crypt,PASSWORD_BCRYPT,$options);
			unset($data['button']);
			unset($data['passwordcon']);
				if ($this->account->update($id,$data) == false) {
					$this->session->set_flashdata('msg-warning','เปลี่ยนรหัสผ่านไม่สำเร็จ');
				}else {
					$this->session->set_flashdata('msg-success','เปลี่ยนรหัสผ่านสำเร็จ');
				}
    		redirect('backend');
		}


	}

	public function delete()
	{
		$id=$this->uri->segment(3);
		if ($this->account->delete($id) == false) {
			$this->session->set_flashdata('msg-warning','ลบข้อมูลไม่สำเร็จ');
		}else {
			$this->session->set_flashdata('msg-success','ลบข้อมูลสำเร็จ');
		}
		redirect('backend');

	}




}
