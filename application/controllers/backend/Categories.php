<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends MY_Controller {
	public function __construct()
    {
        parent::__construct();
		$this->load->model('Categories_model','categories');
        is_logged_in_admin();
    }
	public function index()
	{
        $this->data['categories']=$this->categories->findAll();
		$this->data['css'] = array(
            'assets/css/bootstrap.min.css' ,
            'assets/css/elisyam-1.5.min.css',
			'assets/plugin/css/datatables/datatables.min.css'
        );
		$this->data['js'] = array(
            'assets/js/jquery.min.js' ,
            'assets/js/core.min.js',
			"assets/plugin/js/datatables/datatables.min.js",
	        "assets/plugin/js/datatables/dataTables.buttons.min.js",
	        "assets/plugin/js/datatables/buttons.html5.min.js",
	        "assets/plugin/js/datatables/vfs_fonts.js",
	        "assets/plugin/js/nicescroll/nicescroll.min.js",
	        "assets/plugin/js/app/app.min.js",
			"assets/plugin/js/components/tables/tables.js"
        );
        $this->layout_admin("backend/categories/index",$this->data);
	}
    public function form()
	{
        $id = $this->uri->segment(4);
		$this->data['css'] = array(
			'assets/css/bootstrap.min.css' ,
			'assets/css/elisyam-1.5.min.css',
		);
		$this->data['js'] = array(
			'assets/js/jquery.min.js' ,
			'assets/js/core.min.js',
			'assets/plugin/js/nicescroll/nicescroll.min.js',
			'assets/plugin/js/app/app.min.js'
		);
		if ($id == NULL) {
			$this->data['categories'] = NULL;
            $this->layout_admin("backend/categories/form",$this->data);
        } else {
            $this->data['categories'] = $this->categories->findById($id);
            $this->layout_admin("backend/categories/form",$this->data);
        }

    }
    public function save()
	{
		$id = $this->input->post('id');
		$config = array(
        array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required'
        	)
		);
		$this->form_validation->set_rules($config);
		if($this->form_validation->run()==false) {
			$this->form();
        } else {
			foreach ($this->input->post() as $key => $value) {
				$data[$key] = $this->security->xss_clean($value);
			}
			unset($data['button']);
			if ($data['id'] == NULL) {
				if ($this->categories->save($data) == false) {
					$this->session->set_flashdata('msg-warning','บันทึกไม่สำเร็จ');
				} else {
					$this->session->set_flashdata('msg-success','บันทึกสำเร็จ');
				}
			} else {
				if ($this->categories->update($id,$data) == false) {
					$this->session->set_flashdata('msg-warning','แก้ไขไม่สำเร็จ');
				}else {
					$this->session->set_flashdata('msg-success','แก้ไขสำเร็จ');
				}
			}
			redirect('backend/categories');
		}
	}
    public function delete()
	{
		$id=$this->uri->segment(4);
		if ($this->categories->delete($id) == false) {
			$this->session->set_flashdata('msg-warning','ลบไม่สำเร็จ');
		}else {
			$this->session->set_flashdata('msg-success','ลบสำเร็จ');
		}
		redirect('backend/categories');
	}
}
