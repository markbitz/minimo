<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('Main_model','main');
        is_logged_in_admin();
    }
    public function index()
	{
        $id= $_SESSION['logged_in_admin']['id'];

		$this->data['data']=$this->main->findById($id);

        $this->data['css'] = array(
            'assets/css/bootstrap.min.css' ,
            'assets/css/elisyam-1.5.min.css',
			'assets/css/style.css',
        );
		$this->data['js'] = array(
            'assets/js/jquery.min.js' ,
            'assets/js/core.min.js',
	        'assets/plugin/js/nicescroll/nicescroll.min.js',
            'assets/plugin/js/owl-carousel/owl.carousel.min.js',
	        'assets/plugin/js/app/app.min.js',
			'assets/plugin/js/app/contact/contact.min.js'

        );
            $this->layout_admin("backend/dashboard/index",$this->data);
	}
}
