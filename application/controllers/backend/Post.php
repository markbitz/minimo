<?php
/**
 * This is the summary for a DocBlock.
 * PHP version 5
 * This is the description for a DocBlock. This text may contain
 * multiple lines and even some _markdown_.
 *
 * * Markdown style lists function too
 * * Just try this out once
 *
 * The section after the description contains the tags; which provide
 * structured meta-data concerning the given element.
 *
 * Page-Level DocBlock
 *
 * @category MyCategory
 * @package  MyPackage
 * @author   Mike van Riel <me@mikevanriel.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://example.com/my/bar Documentation of Foo.
 *
 * @return integer Indicates the number of items.
 * @since  1.0
 *
 * Page-Level DocBlock
 */

defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * This is the summary for a DocBlock.
 * PHP version 5
 * This is the description for a DocBlock. This text may contain
 * multiple lines and even some _markdown_.
 *
 * * Markdown style lists function too
 * * Just try this out once
 *
 * The section after the description contains the tags; which provide
 * structured meta-data concerning the given element.
 *
 * Page-Level DocBlock
 *
 * @category MyCategory
 * @package  MyPackage
 * @author   Mike van Riel <me@mikevanriel.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://example.com/my/bar Documentation of Foo.
 *
 * @return integer Indicates the number of items.
 * @since  1.0
 *
 * Page-Level DocBlock
 */
class Post extends MY_Controller
{
    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @return array
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Post_model', 'post');
        $this->load->model('Categories_model', 'categories');
        is_logged_in_admin();
    }
    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @return array
     */
    public function index()
    {
        $this->data['post']=$this->post->findAll();
        $this->data['css'] = array(
        'assets/css/bootstrap.min.css' ,
        'assets/css/elisyam-1.5.min.css',
        'assets/plugin/css/datatables/datatables.min.css'
        );
        $this->data['js'] = array(
        'assets/js/jquery.min.js' ,
        'assets/js/core.min.js',
        'assets/plugin/js/datatables/datatables.min.js',
        'assets/plugin/js/datatables/dataTables.buttons.min.js',
        'assets/plugin/js/datatables/buttons.html5.min.js',
        'assets/plugin/js/datatables/vfs_fonts.js',
        'assets/plugin/js/nicescroll/nicescroll.min.js',
        'assets/plugin/js/app/app.min.js',
        'assets/plugin/js/components/tables/tables.js'
        );
        $this->layout_admin("backend/post/index", $this->data);
    }
    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @return array
     */
    public function form()
    {
        $id = $this->uri->segment(4);
        $this->data['css'] = array(
        'assets/css/bootstrap.min.css' ,
        'assets/css/elisyam-1.5.min.css',
        'assets/plugin/dist/summernote.css'
        );
        $this->data['js'] = array(
        'assets/js/jquery.min.js' ,
        'assets/js/core.min.js',
        'assets/js/my.js',
        'assets/plugin/js/nicescroll/nicescroll.min.js',
        'assets/plugin/js/app/app.min.js',
        'assets/plugin/dist/summernote.js'
        );
        if ($id == null) {
            $this->data['post'] = null;
            $this->data['categories'] = $this->categories->findAll();
            $this->layout_admin("backend/post/form", $this->data);
        } else {
            $this->data['post'] = $this->post->findById($id);
            $this->data['categories'] = $this->categories->findAll();
            $this->layout_admin("backend/post/form", $this->data);
        }

    }
    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @return array
     */
    public function save()
    {
        $id = $this->input->post('id');
        $config = array(
                        array(
                            'field' => 'title',
                            'label' => 'Title',
                            'rules' => 'required'
                        ),
                        array(
                            'field' => 'excerpt',
                            'label' => 'Excerpt',
                            'rules' => 'required'
                        ),
                        array(
                            'field' => 'categories',
                            'label' => 'Categories',
                            'rules' => 'required'
                        ),
                        array(
                            'field' => 'auther',
                            'label' => 'Auther',
                            'rules' => 'required'
                        ),
                        array(
                            'field' => 'cover_thumbnail',
                            'label' => 'Cover_thumbnail',
                            'rules' => 'required'
                        ),
                        array(
                            'field' => 'cover',
                            'label' => 'Cover',
                            'rules' => 'required'
                        ),
                        array(
                            'field' => 'content',
                            'label' => 'Content',
                            'rules' => 'required'
                        ),
                        array(
                            'field' => 'status',
                            'label' => 'Status',
                            'rules' => 'required'
                        ),
                        array(
                            'field' => 'slug',
                            'label' => 'Slug',
                            'rules' => 'required'
                        ),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() == false) {
            $this->form();
        } else {
            foreach ($this->input->post() as $key => $value) {
                $data[$key] = $this->security->xss_clean($value);
            }
            unset($data['files']);
            unset($data['button']);
            if ($data['id'] == null) {
                if ($this->post->save($data) == false) {
                    $this->session->set_flashdata('msg-warning', 'บันทึกไม่สำเร็จ');
                } else {
                    $this->session->set_flashdata('msg-success', 'บันทึกสำเร็จ');
                }
            } else {
                if ($this->post->update($id, $data) == false) {
                    $this->session->set_flashdata('msg-warning', 'แก้ไขไม่สำเร็จ');
                } else {
                    $this->session->set_flashdata('msg-success', 'แก้ไขสำเร็จ');
                }
            }
            redirect('backend/post');
        }
    }
    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @return array
     */
    public function delete()
    {
        $id=$this->uri->segment(4);
        if ($this->post->delete($id) == false) {
            $this->session->set_flashdata('msg-warning', 'ลบไม่สำเร็จ');
        } else {
            $this->session->set_flashdata('msg-success', 'ลบสำเร็จ');
        }
        redirect('backend/post');
    }
}
