<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model','login');
    }

	public function index()
	{

		//$data['data']=$this->main->findAll();
        $css = array(
            'assets/css/bootstrap.min.css' ,
            'assets/css/elisyam-1.5.min.css'
        );
		$js = array(
            'assets/js/jquery.min.js' ,
            'assets/js/core.min.js',
            'assets/plugin/js/nicescroll/nicescroll.min.js' ,
            'assets/plugin/js/app/app.min.js'
        );


		//$this->data = $data;


        $this->load->view("backend/login/index");

	}
    public function loginform()
	{

		//$data['data']=$this->main->findAll();
        $css = array(
            'assets/css/bootstrap.min.css' ,
            'assets/css/elisyam-1.5.min.css'
        );
		$js = array(
            'assets/js/jquery.min.js' ,
            'assets/js/core.min.js',
            'assets/plugin/js/nicescroll/nicescroll.min.js' ,
            'assets/plugin/js/app/app.min.js'
        );


		//$this->data = $data;


        $this->load->view("backend/login/index");

	}
    public function modifine()
	{


		//$data['data']=$this->main->findAll();
        $css = array(
            'assets/css/bootstrap.min.css' ,
            'assets/css/elisyam-1.5.min.css'
        );
		$js = array(
            'assets/js/jquery.min.js' ,
            'assets/js/core.min.js',
            'assets/plugin/js/nicescroll/nicescroll.min.js' ,
            'assets/plugin/js/app/app.min.js'
        );


		//$this->data = $data;


        //$this->load->view("backend/login/index");
        $aData = array(
            'email' => $this->security->xss_clean(
                $this->input->post('email')
            ),
            'password' => $this->security->xss_clean(
                $this->input->post('password')
            )
        );


        $result = $this->login->validate($aData);

        if (!$result) {

            $this->session->set_flashdata(

            //session_start();

                'error',
                'Email or Password is incorrect'
            );

            redirect("login", "refresh");
        } else {

        $lv = $this->session->userdata['logged_in_admin']['level'];
            switch ($lv) {
                case '0':
                    redirect("backend/dashboard", "refresh");
                    break;
				case '1':
	                redirect("backend/dashboard", "refresh");
	                break;
				case '2':
					redirect("backend/dashboard", "refresh");
					break;
                default:
                    echo 'User Incorrect Contact Administartor!!';
                    break;
            } // end switch
        }

	}
    public function logout()
    {
        $id = $this->session->userdata['logged_in_admin']['id'];
        /*$data = array(
            'ipaddress' => null,
            'updatedate' => $this->dateTimeNow
        );
        $save = $this->User_model->update($id, $data);*/
        $this->session->sess_destroy('logged_in_admin');
        redirect("login","refresh");
    }




}
