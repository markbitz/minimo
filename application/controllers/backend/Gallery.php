<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends MY_Controller {
	public function __construct()
    {
        parent::__construct();
		$this->load->model('Gallery_model','gallery');
        is_logged_in_admin();
    }

	public function index()
	{
        $this->data['categories']=$this->categories->findAll();
		$this->data['css'] = array(
            'assets/css/bootstrap.min.css' ,
            'assets/css/elisyam-1.5.min.css',
			'assets/plugin/css/datatables/datatables.min.css'
        );
		$this->data['js'] = array(
            'assets/js/jquery.min.js' ,
            'assets/js/core.min.js',
			"assets/plugin/js/datatables/datatables.min.js",
	        "assets/plugin/js/datatables/dataTables.buttons.min.js",
	        "assets/plugin/js/datatables/jszip.min.js",
	        "assets/plugin/js/datatables/buttons.html5.min.js",
	       	"assets/plugin/js/datatables/pdfmake.min.js",
	        "assets/plugin/js/datatables/vfs_fonts.js",
	        "assets/plugin/js/datatables/buttons.print.min.js",
	        "assets/plugin/js/nicescroll/nicescroll.min.js",
	        "assets/plugin/js/app/app.min.js",
			"assets/plugin/js/components/tables/tables.js"
        );
        $this->layout_admin("backend/categories/list/index",$this->data);
	}
    public function form() {
        $id = $this->uri->segment(4);
        if ($id == NULL) {
            $this->data['categories'] = NULL;
    		$this->data['css'] = array(
                'assets/css/bootstrap.min.css' ,
                'assets/css/elisyam-1.5.min.css',
    			'assets/plugin/css/datatables/datatables.min.css',
                'assets/plugin/css/owl-carousel/owl.carousel.min.css',
                'assets/plugin/css/owl-carousel/owl.theme.min.css'
            );
    		$this->data['js'] = array(
                'assets/js/jquery.min.js' ,
                'assets/js/core.min.js',
    	        'assets/plugin/js/nicescroll/nicescroll.min.js',
                'assets/plugin/js/owl-carousel/owl.carousel.min.js',
    	        'assets/plugin/js/app/app.min.js',
    			'assets/plugin/js/app/contact/contact.min.js'

            );
            $this->layout_admin("backend/categories/create/form",$this->data);
        } else {
            $this->data['categories']=$this->categories->findById($id);
    		$this->data['css'] = array(
                'assets/css/bootstrap.min.css' ,
                'assets/css/elisyam-1.5.min.css'
            );
    		$this->data['js'] = array(
                'assets/js/jquery.min.js' ,
                'assets/js/core.min.js',
                'assets/plugin/js/nicescroll/nicescroll.min.js' ,
                'assets/plugin/js/app/app.min.js',
            );
            $this->layout_admin("backend/categories/create/form",$this->data);
        }

    }
    public function save()
	{
		$config = array(
        array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required'
        	)
		);
		$this->form_validation->set_rules($config);
		if($this->form_validation->run()==false) {
			$this->data['categories'] = NULL;
    		$this->data['css'] = array(
                'assets/css/bootstrap.min.css' ,
                'assets/css/elisyam-1.5.min.css',
    			'assets/plugin/css/datatables/datatables.min.css',
                'assets/plugin/css/owl-carousel/owl.carousel.min.css',
                'assets/plugin/css/owl-carousel/owl.theme.min.css'
            );
    		$this->data['js'] = array(
                'assets/js/jquery.min.js' ,
                'assets/js/core.min.js',
    	        'assets/plugin/js/nicescroll/nicescroll.min.js',
                'assets/plugin/js/owl-carousel/owl.carousel.min.js',
    	        'assets/plugin/js/app/app.min.js',
    			'assets/plugin/js/app/contact/contact.min.js'

            );
            $this->layout_admin("backend/categories/create/form");
        } else {
			$id = $_SESSION['logged_in_admin']['id'];
			$data = array(
				'name' => $this->input->post('name'),
				'id_user' => $id,
				'status'=> $this->input->post('status'),
				'createdate'=>$this->dateTimeNow
            );
			$this->categories->save($data);
			redirect('backend/categories');
		}



	}
	public function edit()
	{
		$id = $this->input->post('id');
		$data = array(
			'name' => $this->input->post('name'),
			'status' => $this->input->post('status'),
			'updatedate'=>$this->dateTimeNow
		);
		$this->categories->update($id,$data);
		redirect('backend/categories');
	}
    public function delete()
	{
		$id=$this->uri->segment(4);
		$this->categories->delete($id);
		redirect('backend/categories');

	}
}
