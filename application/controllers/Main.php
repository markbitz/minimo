<?php
/**
 * This is the summary for a DocBlock.
 * PHP version 5
 * This is the description for a DocBlock. This text may contain
 * multiple lines and even some _markdown_.
 *
 * * Markdown style lists function too
 * * Just try this out once
 *
 * The section after the description contains the tags; which provide
 * structured meta-data concerning the given element.
 *
 * Page-Level DocBlock
 *
 * @category MyCategory
 * @package  MyPackage
 * @author   Mike van Riel <me@mikevanriel.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://example.com/my/bar Documentation of Foo.
 *
 * @return integer Indicates the number of items.
 * @since  1.0
 *
 * Page-Level DocBlock
 */
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * This is the summary for a DocBlock.
 * PHP version 5
 * This is the description for a DocBlock. This text may contain
 * multiple lines and even some _markdown_.
 *
 * * Markdown style lists function too
 * * Just try this out once
 *
 * The section after the description contains the tags; which provide
 * structured meta-data concerning the given element.
 *
 * Page-Level DocBlock
 *
 * @category MyCategory
 * @package  MyPackage
 * @author   Mike van Riel <me@mikevanriel.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://example.com/my/bar Documentation of Foo.
 *
 * @return integer Indicates the number of items.
 * @since  1.0
 *
 * Page-Level DocBlock
 */
class Main extends MY_Controller
{
    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @return array
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('News_model', 'news');
        $this->load->model('Hilight_model', 'hilight');
        $this->load->model('Categories_model', 'categories');
        $this->load->model('Seo_model', 'seo');
        $this->load->model('Post_model', 'post');
    }
    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @return array
     */
    public function index()
    {
        $this->data['post'] = $this->post->findAll();
        $this->data['data_categories'] = $this
            ->categories
            ->findcatenondeleteandstatus();
        $this->data['seo'] = $this->seo->findAll();

        $this->data['css'] = array(
            'assets/css/bootstrapNew.min.css' ,
            'assets/css/bootstrap-theme.min.css' ,
            'assets/css/style.css',
            'assets/plugin/css/all.min.css'
        );
        $this->data['js'] = array(
            'assets/js/jquery.min.js' ,
            'assets/js/bootstrap.min.js',

        );

        $config['base_url'] = base_url().'post';
        $config["total_rows"] = $this->post->record_count();
        $config['per_page'] = 2;
        $config['uri_segment'] = 2;
        $config['use_page_numbers'] = true;
        $config['full_tag_open'] = '<nav aria-label="Page navigation example">
                                    <ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active">
                                    <span class="page-link custompage">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)
                                    </span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tag_close'] = '<span aria-hidden="true"></span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tag_close'] = '</span></li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tag_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tag_close'] = '</span></li>';




        $this->pagination->initialize($config);

        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $this->data["results"] = $this->post
            ->fetch_countries($config["per_page"], $page);
        $this->data["links"] = $this->pagination->create_links();
        $this->layout_frontend("frontend/main/index", $this->data);
    }
    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @return array
     */
    public function photo()
    {
        $this->data['data_news'] = $this->news->findAll();
        $this->data['data_hilight'] = $this->hilight->findAll();
        $this->data['css'] = array(
            'assets/css/bootstrapNew.min.css' ,
            'assets/css/bootstrap-theme.min.css' ,

            'assets/css/style.css',
            'assets/plugin/css/all.min.css'
        );
        $this->data['js'] = array(
            'assets/js/jquery.min.js' ,
            'assets/js/bootstrap.min.js',
        );
            $this->layout_frontend("frontend/photo/index", $this->data);
    }
    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @return array
     */
    public function lifestyle()
    {
        $this->data['data_news'] = $this->news->findAll();
        $this->data['data_hilight'] = $this->hilight->findAll();
        $this->data['css'] = array(
            'assets/css/bootstrapNew.min.css' ,
            'assets/css/bootstrap-theme.min.css' ,
            'assets/css/materialize.css',
            'assets/css/style.css',
            'assets/plugin/css/all.min.css'
        );
        $this->data['js'] = array(
            'assets/js/jquery.min.js' ,
            'assets/js/bootstrap.min.js',
            'assets/js/materialize.min.js'
        );
        $this->layout_frontend("frontend/lifestyle/index", $this->data);
    }
}
