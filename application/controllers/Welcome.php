<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->data['css'] = array(
            'assets/css/bootstrap.min.css' ,
            'assets/css/elisyam-1.5.min.css'
        );
		$this->data['js'] = array(
            'assets/js/jquery.min.js' ,
            'assets/js/core.min.js',
	        'assets/plugin/js/app/app.min.js'


        );
		$this->load->view('welcome_message',$this->data);
	}
}
