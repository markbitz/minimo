<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
/*frontend*/
$route['lifestyle'] = 'main/lifestyle';
$route['photo'] = 'main/photo';
$route['post/(:any)'] = 'main/photo/$1';

$route['backend/categories'] = 'backend/categories/index';
$route['backend/categories/add'] = 'backend/categories/form';
$route['backend/categories/edit/:num'] = 'backend/categories/form/$1';
$route['backend/categories/delete/:num'] = 'backend/categories/delete/$1';

$route['backend/post'] = 'backend/post/index';
$route['backend/post/add'] = 'backend/post/form';
$route['backend/post/edit/:num'] = 'backend/post/form/$1';
$route['backend/post/delete/:num'] = 'backend/post/delete/$1';


$route['backend/seo/add'] = 'backend/seo/form';
$route['backend/seo/edit/:num'] = 'backend/seo/form/$1';
$route['backend/seo/delete/:num'] = 'backend/seo/delete/$1';

$route['backend/add'] = 'backend/account/form';
$route['backend/edit/:num'] = 'backend/account/form/$1';
$route['backend/delete/:num'] = 'backend/account/delete/$1';
$route['backend/reset_password/:num'] = 'backend/account/form_reset_password/$1';

//$route['login'] = 'backend/login/index';
//$route['layout/frontend'] = 'frontend/testlayout/index';
$route['login'] = 'backend/login/index';
$route['login/modifine'] = 'backend/login/modifine';
$route['login/logout'] = 'backend/login/logout';
$route['backend/dashboard'] = 'backend/dashboard/index';
$route['backend'] = 'backend/account/index';
$route['backend/main'] = 'backend/main/profile';
$route['backend/gallery'] = 'backend/main/gallery';


$route['backend/main/gallery/create_gallery'] = 'backend/main/create_gallery_form';
$route['backend/main/create_gallery'] = 'backend/main/create_gallery';
$route['backend/main/gallery/group/:num'] = 'backend/main/gallery_group/$1';
$route['backend/main/gallery/uploads/:num'] = 'backend/main/uploads_form/$1';
$route['backend/main/gallery/uploads'] = 'backend/main/uploads';
$route['backend/main/gallery/uploads/setthumb/:num/:num'] = 'backend/main/setthumb/$1/$2';
$route['backend/user'] = 'backend/user/index';
$route['backend/user/uploads_avatar_form'] = 'backend/user/uploads_avatar_form';
$route['backend/user/gallery'] = 'backend/user/gallery';
$route['backend/user/gallery/group/:num'] = 'backend/user/gallery_group/$1';
$route['backend/user/gallery/create_gallery'] = 'backend/user/create_gallery_form';
$route['backend/user/gallery/uploads/:num'] = 'backend/user/uploads_form/$1';
$route['backend/user/gallery/uploads'] = 'backend/user/uploads';
$route['backend/user/gallery/uploads/setthumb/:num/:num'] = 'backend/user/setthumb/$1/$2';
//$route['backend/user/edit'] = 'backend/user/edit';
$route['backend/superuser'] = 'backend/superuser/index';
$route['backend/superuser/uploads_avatar_form'] = 'backend/superuser/uploads_avatar_form';
$route['backend/superuser/gallery'] = 'backend/superuser/gallery';
$route['backend/superuser/gallery/create_gallery'] = 'backend/superuser/create_gallery_form';
$route['backend/superuser/gallery/uploads/:num'] = 'backend/superuser/uploads_form/$1';
$route['backend/superuser/gallery/group/:num'] = 'backend/superuser/gallery_group/$1';
$route['backend/superuser/gallery/uploads'] = 'backend/superuser/uploads';
$route['backend/superuser/gallery/uploads/setthumb/:num/:num'] = 'backend/superuser/setthumb/$1/$2';
$route['post'] = 'main/index';
$route['post/:num'] = 'main/index/$1';
$route['default_controller'] = 'main';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
